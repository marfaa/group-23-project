# Vision Document For Group 23

## Table of Contents

[Revision History](#revision-history)

1. [Introduction](#introduction)
    1. [Purpose](#purpose)
    2. [Scope](#scope)
    3. [References](#references)
    4. [Overview](#overview)
2. [Positioning](#goals)
    1. [Business Opportunity](#business-opportunity)
    2. [Problem Statement](#problem-statement)
    3. [Product Position Statement](#product-position-statement)
3. [Project Goals](#project-goals)
    1. [Efficiency Goals](#efficiency-goals)
    2. [Result Goals](#result-goals)
    3. [Process Goals](#process-goals)
4. [Stakeholder and User Descriptions](#stakeholder-and-user-descriptions)
    1. [Stakeholder Profiles](#stakeholders-profiles)
    2. [User Profiles](#user-profiles)
    3. [User Environment](#user-environment)
    4. [Stakeholder Profiles](#stakeholder-profiles)
5. [Product Overview](#product-overview)
    1. [Product Perspective](#product-perspective)
    2. [Risk Analysis](#risk-analysis)
    3. [Cost Estimates](#cost-estimates)
6. [Product Features](#product-features)
    1. [Interaction Menu](#interaction-menu)
    2. [Creating Tasks](#creating-tasks)
    3. [Change Status and Priority](#change-status-and-priority)
    4. [Creating Categories](#creating-categories)
    5. [Display List of Tasks](#display-list-of-tasks)
    6. [Reorder Lists](#reorder-lists)
7. [Constraints](#constraints)
8. [Precedence and Priority](#precedence-and-priority)
9. [Requirements](#requirements)
    1. [Product Requirements](#product-requirements)
    2. [Documentation Requirements](#documentation-requirements)

### Revision History
 | Date | Version | Description | Author |
| :--- |:---: | :---: | ---: |
| 07.03.2021 | 0.1 | First Draft | Jusuf, Marthea, Camilla, Viktor & Maria |
| 11.03.2021 | 0.2 | Second Draft  | Viktor |
| 00.00.000 |  |  |  |
| 00.00.000 |  |  |  |

## Introduction

### Purpose

The purpose of this vision document is to define high level needs, features, collect and also analyze the different software requirements for izi-List. It focuses on the different needs of our target users and the capabilities needed by the stakeholders, and explains why these needs exist.

### Scope

The scope of this vision document is set mainly for the implementation but also the design for the software application Izi-list. The features, different uses, the structure and capabilities are also included.

### References

[1] [Lectures from PROG2021](https://ntnu.blackboard.com/ultra/courses/_23068_1/cl/outline) <br>
[2] [Vision Document template](https://ntnu.blackboard.com/ultra/courses/_23068_1/cl/outline) <br>
[3] [BalsamiQ prices for desktop version](https://balsamiq.com/buy/#d) <br>

### Overview

Following in this document:

**Chapter 2 Positioning**

- Gives a description of the business opportunity which states the needs of the project. 
- Table summarizing the problem statement.
- Table summarizing the product statement.

**Chapter 3 Project goals**

Provides several lists of different categories:

- Project goals that explain the objectives set for this project.
- Efficiency goals containing desired impact of the product.
- Result goals with plans for the products feature and quality, as well as plans for the usage of resources and time management.
- Process goals where it’s listed the desired outcomes that the team members will gain from working on this project.

**Chapter 4 Stakeholder and User Descriptions**

- Contains profiles of stakeholders and users with descriptions of the respective problems and the proposed solutions.
- Throughout the chapter there will also be an examination of the marketplace demographic, competitors and a review of noteworthy trends within the relevant market.

**Chapter 5 Product Overview**

- Explores the relations between the product and the user’s environment. 
- A summary of the products capabilities for a more practical method of addressing an issue without going through unnecessary details.
- Assumptions about what the product will depend on
- Measurement of the possible risks and their likelihood
- Quantified and un-quantifiable benefits provided by the product 
- Expected loss of direct costs, which estimates what resource spendage will be reduced by using the product.
- An estimation of the accumulated costs from creating the product
- Comparing the estimated costs with possible benefits
- Listings of licenses and installation requirements

**Chapter 6 Product features**

- Lists the features of the application with a description is intended for both people within and outside the project.

**Chapter 7 Constraints**

- Limits that comes with design, external capabilities and other dependencies

**Chapter 8 Quality Ranges**

- Ranges for the applications performance, robustness, fault tolerance, usability, similar characteristics.

**Chapter 9 Precedence and Priority**

- Table categorizing features by their priority level.

**Chapter 10 Other Product Requirements**

- Containing the following in lists:
- All standards the product must comply to
- The necessary system requirements needed to support the software
- Performance required to support the software
- What is required from the environment

**Chapter 11 Documentation Requirements**

- Describes the purpose and content of the User Manual.
- Describes the available online help.
- Installation Guides, Configuration, and Read Me File containing guidelines and instructions.
- Descriptions of the products labeling and packaging.

**Chapter 12 Feature Attributes**
- Status table over requests regarding the softwares features for discussions within the team.
- Table categorizing features according to the estimated effort requirements.
- Estimation of the probability regarding changes in the software.
- Records of intended features of the software.
- Assignment of different features of the software.
- Sources of different requests made along with the reason for the request.

Chapter 2
## Positioning

### Business Opportunity

Our client has requested an easy, and effective to-do-list application. They are requesting a software where a user is able to register new tasks. When the new task is added, the software should keep all the relevant information like priority, task description, category, status, start and finish date, and deadline. They request that the user also should be able to mark the different tasks as done, change the tasks priority and reorder tasks.

### Problem Statement

| The problem of | Keeping track of different tasks with varying degree of importance |
| affects  | Business companies and casual users with a preference for keeping a schedule for daily tasks  |
| the impact of which is  | Increase efficiency as well as decrease chances of forgetting plans  |
|  a successful solution would be | Providing and an user friendly application that stores and displays tasks  |

### Product Position Statement

| For | Business companies, and casual users |
|---|---|
| Who | Have a an interest in having their plans stored in a  organized structure |
| The izi-List  | is a To-do-list application |
| That | Provides a platform for making new tasks, add information about priority, task description, category, status, start and finish date, and deadline. Mark tasks as done, change the priority of the task and reorder the tasks |
| Unlike | Other to-do-list applications  |
| Our product | Is unique, by providing the users an fast alternative way for creating structured, easy to follow and read to-do lists, where they can reorganize and add tasks as one whishes, and change these accordingly |

<br><br><br>

Chapter 3
## Project Goals

### Efficiency Goals

1. Create a time efficient and structured application for Time Management.
2. Reach the specific target group of specified in 2.2 Product Statement.
3. Increase the company’s personnel productivity by atleast 50%.

### Result Goals

Result goals describe what we want to achieve with the project. It is linked to the result and the end product. The result goals for the project are:
Providing an application with following features:

1. Ability to register different tasks added by user
    - Store registered tasks 
    - Sort tasks, with the option to reorder the list
    - Put tasks into different categories
    - Add a priority level to tasks
    - Provide display the status of each task, with the ability to change it
    - Ability for the user to add different categories
    - Add a start and end date to tasks


2. Provide an application with the following experience :
    - Application is compatible with the user’s environment
    - Usage of the applications features can easily be understood


3. Focus on the target group. We plan to focus on the needs and wishes of the target group to build a product beneficial for users within this group. To achieve this, we must research well and make sure that the product is tailored for the chosen target group.

4. Meeting submission deadlines. It is important that all assignments are submitted on time. Our goal is for all assignments to be approved without changing any deadlines. To achieve this, it is important that everyone adheres to their roles. It is also important that everyone attends meetings, in addition to working on their assigned tasks at home. The secretary holds the responsibility of having an overview of submission deadlines.

### Process Goals

Throughout the process of making this project, we have some goals we wish to complete.
Such as:

- Getting to know the team members better.We want to build trust in each other and learn from each other. To achieve this we want to have weekly meetings, an open dialog and to listen to each other’s suggestions.
- End up with a good product that all the members are happy with.
- Pass the subject
- Learn, and improve technical abileties
- Build and utilize each members strengths 

Chapter 4
## Stakeholder and User Description

### Stakeholder Profiles

A stakeholder is a party that has an interest in the development of the software. The identified stakeholders include the development team and a client that is also an expert, as well as a client/end-users.

**Table 4.1 Stakeholder Summary**

| Name | Description | Role/responsibility |
|:---|---|---:|
| Peder Hovdan Andresen | Expert Advisor/Client | As an advisor, Peder monitors the project’s progress and gives feedback on how to improve the project, the progress and the organizational administration of the project. |
| Jusuf-Gerey Beslanovitsj Kartoev <br><br> Maria Franko Aas <br><br> Marthea Lersbryggen Wichstad <br><br> Camilla Grønvold <br><br> Viktor Grande Røssnes | Members of the Development Team and the responsibilites everyone have | The development of the product and writing relevant documents.<br> Documentation of individual working hours and working hours.<br> Cooperation and communication between the group members and with other stakeholders and possible customers.<br> Planning the progress of the project and ensuring that the project costs does not exceed the budget. |

### User Profiles
End Users
|Representative  | Stakeholder: Development Team |
|:---|---:|
|Description  | Interest for keeping a scheduel over tasks |
|Type  | Casual Users|
|Responsibilities  | None |
|Success Criteria  | Use the application to create and list tasks |
|Involvement  | Not involved |
|Deliverables  |  |
|Comments / Issues  |  |

### User Environment

The application is meant to be used as a desktop software. It will be compatible with computer devices, and the operating systems: Windows, Linux and Mac OS.

### Stakeholder Profiles

**Client**

|Representative  |Peder Hovdan Andersen  |
|:---|---:|
|Description  |  |
|Type  |Business   |
|Responsibilities  |- Work is submitted within the deadline<br>- The application meets the requirements agreed upon<br>- The work process is documented (e.g. time table)  |
|Involvement  |- 1st iteration: Collaboration Agreement document, Vision document, early prototype made in BalsamiQ and the results from the usability test<br>- 2nd iteration<br>- 3nd iteration|
|Comments / Issues  |  |

**Development Team**

|Representative |Maria Franko Aas<br><br>Camilla Grønvold<br><br>Jusuf Kartoev<br><br>Marthea Lersbryggen Wichstad<br><br>Viktor Grande Røssnes |
|:---|---:|
|Description  |A team responsible for the development of the software  |
|Type  | Development Team |
|Responsibilities  | Planning and development of project <br><br> Developing prototypes <br><br> User testing <br><br> Documentation in the project and for the software|
|Success Criteria  |  |
|Involvement  | The application meets the specified requirements<br><br> Deliver within the deadlines|
|Deliverables  |  |
|Comments / Issues  |  |

Chapter 5
## Product Overview

### Product Perspective

izi-List is intended to be a self-contained program installed/available(?) on a user’s personal computer or laptop. The application will store, edit and read from text files generated within the local environment where izi-List is installed.

### Risk Analysis

Here we document conditions that may prevent the project to succeed. In addition, we make an assessment of the likelihood that the various risks occurring, and the consequences if they occur.

The risk factors we need to worry about are the ones in the lower right corner: likely and with serious consequences. Those we do not have to worry about are those located in the upper left corner: low risk, small impacts.

With this diagram, we briefly describe each risk factor:

(1 = lowest, 9 = highest)
| ID | Description | Likeability | Consequence | Probability (Likeability * consequence |
|:---|---|---|---|---:|
| 1 | A team member who is absent/sick | 1 | 1 | 1 |
| 2 | Unable to deliver in time because of technical issues | 2 | 3 | 6 |
| 3 | Conflicts that arises within the team | 1 | 2 | 2 |
| 4 | A lack of subject/technical experience | 3 | 1 | 3 |
| 5 | Misunderstanding within the team | 2 | 2 | 4 |

| Consequence Probability | Consequence = 1 | Consequence = 2 | Consequence = 3 |
|:---|---|---|---:|
| Very Unlikely = 1 | ID1 | ID3 |  |
| Likely        = 2 |  | ID5 | ID2 |
| Very Likely   = 3 | ID4 |  |  |

### Cost Estimates

|  | Number of people | Hourly salary (NOK) | Hours pr. person| Total salary (NOK)  |  
|:---|---|---|---|---:|
| Development | 5 | 1 000 ,- | 50 | 250 000 ,- |
| Total |  |  |  | 250 000 ,- |

|  | Price (NOK) |
|---|---|
| BalsamiQ for 5 users | 3 680 ,- |
| Total | 3 680 ,- |

Chapter 6
## Product Features

### Interaction Menu

Text which explains what the different command inputs will do on different screens. 

### Creating Tasks

The user is able to create new tasks by writing in a description of a task. To this task it will be added a priority rating, category, status, start time and deadline. 

### Change Status and Priority

The tasks registered by the user can be selected and given the option to change the status or the priority of the selected task. 

### Creating Categories

An ability for the user to create their own categories they can later tag the tasks with.

### Display List of Tasks

Ability to display registered tasks as a list with their information

### Reorder Lists

Ability to reorder the list of tasks (e.g. by priority, date) 

Chapter 7
## Constraints

This application will be created within the constraints of what can be made using the c++ language with software such as CodeBlocks or Visual Studio. Meaning that the advancement in how the user can interact with the software, and the graphic capabilities are limited to the terminal text.

The application is intended for usage on a personal computer, stationary or laptop, and will not be available on other types of end devices.

Chapter 8
## Precedence and Priority
|Priority Level|Feature|
|:---|---:|
| High priority | Creating tasks<br><br>Tag tasks with categories<br><br>Set and change status of tasks<br><br>Ability to reorder tasks by date or priority |
| Medium priority | User ability to create categories<br><br>Graphic design displayed in the application |
| Low priority | More options to sort tasks by than stated in the priority feature within “High priority” |

Chapter 9
## Requirements

### Product Requirements
The product should be a program adopting Graphic User Interface (GUI) following WCAG 2.1 Principle 1: Perceivable. Thus, the following requirements are to be met:
- the content is structured logically and can be navigated and read by a screen reader.
- use the proper markup for every feature, so the relationships between content are defined properly.
- not use colour as the only way to explain or distinguish something.
- use text colours that show up clearly against the background colour.

In addition, to ensure high usability and a good user experience the application is required to adopt Don Norman’s principles of interaction design. This includes the following principles:
- Visibility: The more visible an element is, the more likely users will know about them and how to use them.
- Feedback: Feedback should make it clear to the user what action has been taken and what has been accomplished.
- Constraints: Limiting the range of the possible interactions guides the user to the appropriate next action, this can be done through a simple interface.
- Mapping: It should be a clear relationship between the controls and the effect they have on the program.
- Consistency: This refers to having similar operations and similar elements for doing similar tasks.
- Affordance: The attributes of objects should allow people to know how to use them.

To ensure that the principles listed above are met, usability tests will be carried out twice. The first test will be a user test including a working prototype, while the second usability test will be centered around the minimum value product.
- Prototype: The prototype will be designed using Balsamiq to show the design and main interaction of the product.
- Minimum Value Product (MVP): The MVP will be programmed in C++ and will be a working prototype with just enough functionality to work as a proof of concept. Nonetheless, the MVP should not contain known bugs. Ideas for functionality that is not yet implemented will presented along with the MVP.

### Documentation Requirements
- Domain Model
- Vision document
- Test report
- Wireframes
- Sequence diagram
- User case diagram
- Meeting notes and meeting invitations
- Time sheets
- Personal notes on the progress
