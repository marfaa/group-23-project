# Collaboration Agreement For Group 23

## Table of Contents
1. [Introduction](#introduction)
2. [Goals](#goals)
    1. [Impact goals](#impact-goals)
    2. [Result goals](#result-goals)
3. [Role responsibilities](#role-responsibilities)
    1. [Role Table](#role-table)
4. [Procedures for the teamwork](#procedures-for-the-teamwork)
5. [Interaction](#interaction)
6. [Signatures](#signatures)

## Introduction

The collaboration agreement defines the goals, role responsibilities, procedures and guidelines for interaction and cooperation within the team. The agreement is written by the team members and represents the teams interpretation of these parameters and how to fulfill the goals of the project.
In this collaboration agreement the overall goal for this project, and the methods we use to achieve this, is listed. This document will contain information of the different team roles, as well as the tasks associated with those roles. We aim to present a structured approach for our project with an agreement on how we plan to collaborate and carry out assigned tasks.

## Goals

### Impact goals
Impact goals refers to the long-term goals for the group. Our impact goals for this project are:
1.   Get to know each other. We want to build trust in each other and learn from each other. To achieve this we want to have weekly meetings, an open dialog and to listen to each other’s suggestions.  
2.   Learn. We want the project to be a learning experience that gives us knowledge we can utilize during our studies. This can be done by working hard and thoroughly, with the focal point being the group as a whole and not individual tasks. To achieve this we must work hard and thoroughly, and everyone needs to be included in all of the assignments. 
3.  Work well as a team. We all have a strong desire to cooperate well as a team. By working well together, the result will far exceed what we would do individually. To achieve this, we must support, help, and allow each other to develop.
4.  Punctuality. It is important that everyone in the group attends the group meetings punctually. To achieve this, we must be clear about where and when to show up, in addition, everyone has a responsibility to show up when required.
5.  Collaborate effectively and utilize each other's strengths so that we get the best end result possible. There is a large workload, so it is important that everyone works efficiently with their designated tasks. To achieve this, we must work in a structured manner and take deadlines and attendance seriously.
6.  Have fun! It is important that everyone enjoys the project and thrives. To achieve this, we must encourage each other, and everyone should do their best to stay positive.


### Result goals

Result goals describe what we want to achieve with the project. It is linked to the result and the end product. The result goals for the project are:
1.   Deliver a good product. We all have high expectations of demands on ourselves and the group, and want to deliver a product we are proud of. It is therefore important that everyone is happy with the end result. To achieve this, everyone must contribute what they can and manage to ensure the best possible result.
2.   Focus on the target group. We plan to focus on the needs and wishes of the target group to build a product beneficial for users within this group. To achieve this, we must research well and make sure that the product is tailored for the chosen target group.
3.   Meeting submission deadlines. It is important that all assignments are submitted on time. Our goal is for all assignments to be approved without changing any deadlines. To achieve this, it is important that everyone adheres to their roles. It is also important that everyone attends the meetings, in addition to working on their assigned tasks at home. The secretary holds the responsibility of having an overview of submission deadlines.

## Role responsibilities

1. Leadership – Project leader
- Assign tasks to group members
- Be up to date on the progress of each task and what can be done to assist each team member in their work.
- Lead the meetings.?

2. Organisation of meetings (invitations, preparations, minutes, management)
- Book group rooms/Arrange digital meetings.
- Decide the most suitable time for the meeting
- Notify other members when the meeting is being held

3. Archive/ Document responsible
- Quality control of the relevant documents for the week
- Be in dialogue with the teaching assistant about the requirements
- In charge of submitting work within the deadline

4. Minutes of meetings
- Document the time and date of the meeting
- Document what was discussed/worked on during the meeting
- Submitting the minutes

5. Preparation of meetings
- Have a brief list of lectures/parts of the curriculum that will be relevant to the weekly tasks
- Upload word documents to markdown if needed

### Role Table
 | Week | Leader | Meeting Organizer | Archive Responsible | Meeting Taker | Curriculum coordinator|
| :--- |:---: | :---: | :---: | :---: | ---: |
| 9 | Camilla | Jusuf | Marthea | Viktor | Maria |
| 10 | Jusuf | Marthea | Viktor | Maria | Camilla |
| 11 | Marthea | Viktor | Maria | Camilla | Jusuf |
| 12 | Viktor | Maria | Camilla | Jusuf | Marthea |
| 13 | Maria | Camilla | Jusuf | Marthea | Viktor |
| 14 | Camilla | Jusuf | Marthea | Viktor | Maria |
| 15 | Jusuf | Marthea | Viktor | Maria | Camilla |
| 16 | Marthea | Viktor | Maria | Camilla | Jusuf |
| 17 | Viktor | Maria | Camilla | Jusuf | Marthea |
| 18 | Maria | Camilla | Jusuf | Marthea | Viktor |

## Procedures for the teamwork

1. Meetings

Notice of a meeting will be the task of the person responsible for this week's meeting. We will have one digital meeting a week. This The whole group has been agreed with the whole group that will that the meetings will take place on Thursdays, unless otherwise decided. The meeting organizer of the week is responsible for convening the meetings and everything related to the meetings around. The meeting will be held in called into the Facebook group through a phone call. If we need more meetings or something arises then this can be done happen digitally on Teams or Discord.

2. Notification in case of absence or other incidents

If you for some reason can not attend the meeting at the scheduled time or can not attend the meeting at all, this must be notified as early as possible in the Facebook group. The minutes taker writes down if anyone is absent. If you are unable to attend, it is still important that you still do your tasks. It is also important to read through the minutes from the meeting to see what was done and discussed in the meeting, as well as what should be done.

3. Documents

We mainly use Google Docs for all our documents as we can edit them collectively there. This is more transparent and eliminates the risk of losing documents as everything is stored online. We will also be using git actively throughout this project. While using git, different versions of the documents will be saved, this way you can always go back and see the changes that has been made, and so on. 

4. Policy for monitoring tasks

Each member must stay within the weekly schedule plan. If a member for some reason is not able to uphold the deadline, the person should contact the rest of the group immediately. 

5. Submission of team work

The document responsible for the week will submit possible assignments on Blackboard or in Git. That way, it will be clear who holds this responsibility. Texts must always be quality checked before they are submitted. Any submission deadlines within the group are handed in by each team member in Google Docs.

6. Conflict management

If a conflict should arise that cannot be resolved, we will refer back to the collaboration agreement and the current week's project leader makes the final decision. 

## Interaction

1. Attendance and preparation

Accepted meeting time for group meetings and lectures. 
It is required that each group member attend all the meetings that will be held. There should be appropriate meeting preparation of each student according to the “Preparation of meetings”. It must be agreed on how the work is to be done further and how far each person has come, and take appropriate actions in comparison to the week-plan.  

Rules for the preparation, implementation and follow-up meetings
Follow-up meetings must ensure that things get done according to the week-plan. If a member is struggling to keep up his/her deadlines, they should immediately contact the rest of the group and inform them either from the facebook group or the discord chat. The group should come to an agreement on how the work should be distributed between each other so that all the deadlines can be upheld in time. 

2. Presence and commitment

What about using PC for entertainment while work is in progress.
Each member must understand and support the team's decisions and recommendations. There will be maximum productivity if all the members accept and stand behind the decisions the team made. That also includes the member(s) that initially proposed or opposed a different kind of idea.

3. How to support each other

To work well together, it is important to know your team members. Having fun and playing games are important to keep a creative mindset. It is also important to have a positive and supportive mindset throughout the project. 

Procedures to comment on each other’s work.
Constructive comments are an important aspect of every group project and represent a learning experience. The comments should always focus on being constructive and positive aspects of the work should be included. The comments shall not focus on personal matters and should be as professional as possible.
 
4. Disagreement, breach of contract

How to handle disagreements, conflict handling
If disagreements or conflicts should arise, the current team leader holds the responsibility of conflict management to de-escalate and resolve the conflict. This should be done through dialogue where each part gets to explain their point of view. The team leader tries to find a compromise between the parts of the disagreement. If dialogues should fail to resolve the conflict, a last resort should be to contact the teaching assistant or professor to get help to resolve the conflict.

How to deal with team members who do not comply with the agreement. 
If a team member does not comply with the content of this agreement, the team leader holds the responsibility of notifying and reminding the team member in question about the content of this agreement. A discussion of why the team member did not comply should follow to understand the underlying factors of the problem, to help and assist the member in their work. If the member in question still does not adhere to the agreement, the member will be given three formal warnings before the teaching assistant or professor shall be contacted and reasonable measures shall be adopted.

## Signatures
 
  Maria Franko Aas&nbsp;&nbsp;&nbsp;Camilla Grønvold&nbsp;&nbsp;&nbsp;Jusuf Kartoev  


 Marthea Lersbryggen Wichstad&nbsp;&nbsp;&nbsp;Viktor Grande Røssnes
