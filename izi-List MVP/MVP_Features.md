# MVP Features
 
Some information on what features the izi-List MVP have, and what data members are used in these features.
 
## Task as an object
 
The tasks that are created by the user will be stored as objects. To do this a class called Task is set up, which stores different values like day, priority, description etc. This means that when a user adds a task to the list a new object is created with it's own values determined by the user. 
More information on the values mentioned below.
 
## Selection of task values
 
For the minimal viable product we made a software that could store the most essential parts of a task from user input.
These parts are the following:
 
 - Description: To create a reminder for a task, it's important that the software is able to store information on what needs to be done.
 - Date: In the future izi-List will use dates to signify the start or deadline of a task, so it was essential to have a baseline function of storing a date, and be able to add this to a task.
 - Priority: Giving a task a priority rating tells the user how important any stored tasks are. The rating system is between 1 and 3, so for instance if the user gives a rating of 2 it will show up in the lists of tasks as Priority [2/3].
 - Status: This is a boolean value that will be used to notify the user if the task is completed or not. This also serves the purpose of a value that can be changed after a task is added.
 
## User interface
 
The interface is kept simple, so the software is easy to navigate when the user is adding the most essential parts of a task in a to-do list. 
When the program starts the first thing that the user sees is text above saying "TO DO LIST EMPTY", this will change as the user adds tasks and a list will be printed instead, where all tasks have a designated number. Below this first text some options are printed, these tell the user what input causes what action. These actions create a new task, mark a created task as completed and quit.
 
The available actions:
 - The "New task" action can be initiated whenever, and the user chooses all the information that can be added, except status which will be set as false/"not completed". When this action is finished the user is sent back to the "start screen" with the task list updated automatically.
 - The "Mark task as completed" action asks to choose a task from an interval representing the different designated task numbers. When an existing task is chosen the user is sent back to the "start screen" with the task status updated.
 - "Quit" prints out a goodbye message and the software closes

