#ifndef __ALLCONSTANTS_H
#define __ALLCONSTANTS_H

#include <iostream>

const int   MINDAY = 1;            //Minimum date for days
const int   MAXDAY = 31;           //Maximum date for days
const int   MINMONTH = 1;          //Minimum date for months
const int   MAXMONTH = 12;         //Maximum date for months
const int   MINYEAR  = 2021;       //Minimum date for years
const int   MAXYEAR  = 2050;       //Maximum date for years

const int   MINPRIO  = 1;          //Lowest priority rating
const int   MAXPRIO  = 3;          //Highest priority rating

const int   MAXCHAR  = 200;        //Maximum number of characters that can
                                    //be read.

#endif

