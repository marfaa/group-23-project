#include "AllConstants.h"

#include <string>
#include <iomanip>  //setw
#include <vector>
#include <iostream> //cout, cin
#include <cstdlib>  //atoi, flush

using namespace std;

class Task
{
private:
    int     day,
            month,
            year,
            priority;

    string  description;

    bool    status;

public:
        Task();
   void readTask();
   void setPriority();
   void changeStatus();
   void printTask() const;
};

vector<Task*> allTasks; ///< all added tasks.

void writeTasks();
void addTask();
void writeOptions();
void completedTask();
void drawLine(const int length);

//Borrowed function, more information in the functions definition.
int readInt(const char* text, const int min, const int max);

int main()
{
    int choice;  //User input to navigate the switch menu

    //Sets up a do-while loop
    do
    {
      //Prints out the tasks (if there are any).
         drawLine(80);
        writeTasks();
         drawLine(80);

    //Prints out the aviable options
        writeOptions();
         drawLine(80);

    //User is asked to type in one of the options
        choice = readInt("\nType in choice", 1, 3);

    //A switch statment to send the program to the chosen option
        switch(choice)
         {
           case 1: addTask();         break;
           case 2: completedTask();   break;
         }

   //As long as the user doesn't choose option 3 the do-while loop continues.
    } while (choice != 3);

   //Clears the screen when out of the do-while loop.
    cout << flush; system("CLS");
   //Prints out a goodbye-message.
    cout << "\n\n\tizi-List is closing...\n\t\tGoodbye!\n\n";
    return 0;
}

// ---------------------------------------------------------------------------
//      DEFINITION OF CLASS TASK FUNCTIONS
// ---------------------------------------------------------------------------

/**
* Initiates the object
*
* @see Task::readTask()
* @see Task::setPriority()
*/
Task::Task()
{
    //Reads inn content for the task.
    readTask();
    setPriority();

    //sets the status to false ("not completed").
    status = false;
}

/**
* Reads the description and date for a task
*/
void Task::readTask()
{
    cout << "\nWhat is the task?: ";
    getline(cin,description);

    cout << "\nType inn the date:";
    day = readInt("\nDay", MINDAY, MAXDAY);
    month = readInt("\nMonth", MINMONTH, MAXMONTH);
    year = readInt("\nYear", MINYEAR, MAXYEAR);

}

/**
* Gives the task a priority rating.
*/
void Task::setPriority()
{
    //User asked choose the priority within an interval.
    priority = readInt("\nSet a priority level", MINPRIO, MAXPRIO);
}

/**
* Changes the boolean value to 'status' too true
*/
void Task::changeStatus()
{
    status = true;
}

/**
* Prints out the tasks information
*/
void Task::printTask() const
{
            //Prints out the set day.
    cout << ((day < 10) ? "0" : "") << day << "." <<
            //Prints out the set month.
            ((month < 10) ? "0" : "") << month << "." << year <<
            //Prints out the priority rating.
            " | Priority [" << priority << '/' << MAXPRIO << ']' <<
            //Prints out the task description.
            " |  " << setw(10) << left << description <<
            //Prints out the tasks status.
            "\t(" << ((status) ? "" : "not ") << "completed)";

}

// ---------------------------------------------------------------------------
//      DEFINITION OF OTHER FUNCTIONS
// ---------------------------------------------------------------------------

/**
* Adds a new task
*
* @see Task::Task()
*/
void addTask()
{
    Task* newTask;

    //Clears the screen and prints out a headline for the function.
    cout << flush; system("CLS");
    cout << "\n\tNEW TASK"; drawLine(40);

    //Creates a new Task object and pushes it to the back of the vector.
    newTask = new Task();
    allTasks.push_back(newTask);

    //Clears the screen.
    cout << flush; system("CLS");
}

/**
* Changes the status of a chosen task
*
* @see Task::changeStatus()
*/
void completedTask()
{
    int selected; //User selects which task to change.

    //Sees if there are any tasks registered.
    if (!allTasks.empty())
    {
    //User is asked to choose a task by their task number.
     selected = readInt("\nSelect task by typing in the task number"
                        "", 1, allTasks.size());

    //When given a valid input the status changes.
     allTasks[selected-1]->changeStatus();
    }

    //Clears the screen
    cout << flush; system("CLS");
}

/**
* Writes out all registered tasks
*
* @see Task::printTask()
*/
void writeTasks()
{
    if (allTasks.size())
    {
        cout << "\n\tTO DO"; drawLine(80); cout << '\n';

        for (unsigned int i = 0; i < allTasks.size(); i++)
         {
           cout << "\n[Task nr." << i+1 << "]\t";
             allTasks[i]->printTask();
         }

    } else cout << "\n\tTO DO LIST EMPTY";
}

/**
* Writes out the options for the start menu
*/
void writeOptions()
{
    cout << "\nOptions:" << "\t 1 - New task | 2 - Mark task as completed | 3 - Quit";
}

/**
* Draws a line inbetween texts
*
* @param length - given length og the line
*/
void drawLine(const int length)
{
    cout << '\n';
    //Sets up a for-loop that prints '_' the amount given.
    for (int i = 0; i < length; i++) cout << '_';
}

// ---------------------------------------------------------------------------
//      BORROWED FUNCTIONS
// ---------------------------------------------------------------------------

/**
* Borrowed function given in the PROG1003 class, used to read and return
* an integer within an given interval. The function also checks if the user
* has typed in a letter.
*
* @param text - given text that prints out before reading the number.
* @param min - minimum allowed integer.
* @param max - maximum allowed integer.
*
* @return the integer typed in by the user, as long as it passes the checks.
*/
int readInt(const char* text, const int min, const int max)
{
    char buffer[MAXCHAR]; //Used to read input so it can be checked for letters.
    int  number;          //The value returned if possible.
    bool error;           //Check for errors in input.

    //Sets up a do-while loop.
    do {
        error = false;

    //Prints out the given text and minimum/maximum integer.
        cout << text << " (" << min << " - " << max << "):  ";
        cin.getline(buffer, MAXCHAR);

    //Converts buffer to an int value as long as the input consists of integers.
        number = atoi(buffer);

    //If non-integers have been found.
        if (number == 0 && buffer[0] != '0')
        // Error is set to true and a messange is printed.
        {  error = true; cout << "\nPlease enter a number\n\n";  }

    //If the number is under minimum or over maximum.
        else if (number < min || number > max)
        //Error is set to true and a message is printed.
        { error = true; cout << "\nPlease enter a number within the interval"
                             << " (minimum - maximum)\n\n";  }

      //As long as an error have been found, the do-while loop starts again.
    } while (error);

    //If no errors have been found the input is returned.
    return number;
}
