# Prototype report

**1 Prototype version 1**  
As defined in our Vision Document, our product is a to-do-list application called izi-List. The user can add a new task, add a new category, edit the tasks, print out the category list, and change the sorting of the to-do-list. We made the prototype very simple, and close to what we will be able to program in C++. 

**1.1 Frontpage**  
The user is greeted at the front page by their to-do-list, sorted by priority for each day. Each task contains name, description, starting time and ending time, priority, and category. Underneath their to-do-list, it is a menu of all the choices they can do. This includes adding a new task, add a new category, edit the tasks, print out the category list, and change the sorting of the to-do-list. Lastly, the user can write their input choice. 

**1.2 New task**  
To add a new task the user needs to fill out task name, description, data, starting time, ending time, priority and category. If the desired category does not exsist they need to add it, by filling out the category name. Once done they are taken back to the main page.

**1.3 New category**  
To add a new category the user only has to fill out the titel of the category. Once this is done it is taken back to the main page.

**1.4 Edit task**  
When the user types that they want to edit a task they will first be asked the number of the task they want to edit. The current values of the task wil be printed out, as well as a form for the user to input the changes. Once filled out they are taken back to the main page.

**1.5 Category list**  
In the category list the user can see all the available categories. The user gets asked to type in the number of witch category they wish to list out the existing tasks. All the tasks with description for the chosen category is then printet out on the screen. Once done they are taken back to the main page.

**1.6 Change sorting**  
For the user to change the sorting of the to-do-list they are presented with two choises. They can either sort them by priority or time. The user inputs their choice, and they will be taken back to the main page, where all the tasks are sortet by their desired wish.  

**1.7 Quit**  
When the user quits the application a short goodbye message appears befor the program shuts down.

**2 User test**  
After we made the first prototype, we made a user test to get feedback on how we can improve our program. We decided to have a classic user test. The purpose of the user test was to collect feedback on the application from our target group and also to get some answers to our own concerns about the program. The purpose of the user test is to check:
* Navigation on the site
* The design of the application
* If it was user friendly for all target groups
* If the feedback from the program is clear and satisfying
* If the users encountered any errors
* To possibly get some new improvements/changes

Because of social distancing, we had to make the user test online. We made a survey through Google Forms, which made it easy to share the link and easy for our participants to submit their answers. You can find the [survey](https://forms.gle/4eH725nDyC2yDJBb7) here for a closer look.

**2.1 Participants**  
We asked nine people in total. We want our application to be used by as many people as possible, so we focused on having diversity in our testing group. We wanted to get feedback from people that have experience with programming applications like this, as well as people with no experience. We discovered that this was very useful, as it provided us with different points of veiws on the program. 

The pie charts below shows the gender and age distribution from our test. 

**Chart 2.1.A - Pie chart of gender distribution**

![image](C:\Users\Admin\programvareutvikling/gender.png)

**Chart 2.1.B - Pie chart of age distribution**

![image](C:\Users\Admin\programvareutvikling/age.png)

We also asked if they look at them selves as an advanced techology user, and this is what they answered. 

**Chart 2.1.C - Pie chart of users technology skills**

![image](C:\Users\Admin\programvareutvikling/skills.png)

When we looked at all the answers individually we could see some similarities in the dissatisfactions from the different groups, witch was very interesting - and very helpful. 

**2.2 The questions**  
We started the survey by asking some personal questions to the user. This helped us get a better picture of our participants, and we could also easily see which problems the different groups had. 

We based our questions on our own concerns, in addition to some general feedback questions about the application. We divided the questions into 5 main categories; navigation, appearance, usability, interaction feedback, and errors. We wanted our participants to reflect on their own as much as possible. We tried to avoid simple yes/no answers, but we still wanted to give them some diversity to hold onto their attention.

**3 Test results**  
The results from the user test are presented and discussed here. We have collected the results from the five categories that there were questions about, and made a general approval rate. This can help us see on witch areas our application needs improvement. 

**Table 3.A - User Approval of Each Test Category**

![image](C:\Users\Admin\programvareutvikling/table1.jpg) 

*The results were calculated by adding the answers from the questions with a 1-5 answer, multiplying these by 100 and dividing it by the possible full score to find the procentage. 
**The results were calculated by Google forms or Excel itself

**3.1 Frequently given feedback**
Unfortunately, some people gave us negative feedback on the appearance, more specific that they wanted more colors. Sadly this is not something that we can change when programming in C++. This is something that we should have made more clear for the participants before the user test. 

We also got a general feeling that the program was a bit messy and hard to understand for first-time users. This is something we want to improve for the next prototype. 

**3.2 Future changes**
To sum up we are very happy with the survey for our first prototype. We learned a lot, both on how we can improve our application, but also on how we can improve the user test for next time. Here are some bulletpoints on what we take with is from the usertest, moving forward:
* We need to make the application more clear and user-friendly. As mentioned earlier we want this to be a program that is easy to use for everyone and not just people with experience in programming. This can be improved by adding more interaction feedback and add space/tabs to make the application more clear.
* From one participant we got a suggestion to be able to check off a task when finished. The task will then be deleted. This was a great tip that we will definitely try to add to the new prototype.
* Is Q needed, or could it be closed simply by clicking out the program by X- ing the top right corner?
* When editing a task, do we need to fill in everything again, or can we only edit the categories we want?
* Changes to the survey: Add the question “what did you think of the main page?”. Add a longer description of the program, and how its made before the test.