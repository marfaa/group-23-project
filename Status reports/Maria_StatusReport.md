# Status reports(log)

## Week 8:
We had our first meeting with our teaching assistant, Peder. We agreed on the meeting dates and time, made our GitLab project, and made all the templates for the time list, etc. We started to write the Collaboration Agreement and started to plan how we would distribute the team roles. 

The plan for next week is to write the first interaction report.

## Week 9:
Group meeting where we started planning how we were going to do the first interaction assignment. Everyone was given tasks to do within the meeting on Thursday. I was going to make the user test and prototype report. 

I could not start with my tasks until Viktor was done with the prototype. In the meantime, I updated and organized all my time lists, logs, and also made the template for the prototype report and user test. I also started writing down possible questions for the test. I had some questions for Peder about the report, which I wrote down. 

Group meeting with teaching assistant Peder. He answered all of our questions and showed us some pictures from his first interaction. After the meeting, I started making the questions for the user test. I showed several versions to the group, and they gave me their feedback. When the test was finished everyone shared it with their friends and family.  

## Week 10:
Worked on the prototype report and still tried to share the user test with friends and family. Finished the prototype report.

Wrote the prototype report in markdown and uploaded it to the Git project. I had some problems with the picture files of the charts and tables. Sent a message to Peder, and he said that he will show me how at our next meeting. Finished my log and time list for this week, and uploaded it to GitLab. 

## Week 11:
We had a client meeting with Ali, where he gave us feedback on the first iteration. I was the Minutes taker this week. After the meeting, I updated the Prototype Report after feedback from Ali and Peder. I also added milestones under Issues in our GitLab project. 

I also prepared for next week when I am group leader. 

## Week 12:
My week as group leader. I started the week by sending out a group message with a status update, what we need to do forward, and assigned some small assignments for everyone. 

DevOPS week. 

## Week 13: 
Easter holiday.

## Week 14:
Group meeting. Updated the 1st iteration after feedback from Ali and Peder. Also fixed the time list template and updated files to GitLab. 

## Week 15: 
Second client meeting with Ali. I also wrote the meeting minutes. After the meeting, I prepared for the next weeks and the final submission. 

## Week 16:
Was not able to attend the group meeting this week. The next day I used some time reading the meeting minutes and catching up. We had not yet assigned tasks for the last assignment, so I spent my time fixing the Gantt Chart and uploading it to GitLab.

## Week 17:
Group meeting on Monday where we assigned tasks for the last submission. I got the Main Report with Marthea. For the rest of the week, I worked very hard on this. On Friday I made the last updates on the report and also fixed some other technical issues before submitting it. I also finished my timelist, status report and the self reflection report.  
