# Status Report

## Week 8:

One of the first things we all figured out was which day and what time we would have our weekly group meetings with our teaching assistant (TA). We all agreed that at 10 AM on Thursdays would fit best for everyone. We had our first group meeting 25.02.21, where we asked the TA some questions about the collaboration agreement. 



## Week 9:

From our meeting with the TA the week before we got some feedback on the collaboration agreement, where we should make some changes. This week I was given the task to make the changes and add a few points to the Collaboration Agreement. I started working on the Collaboration Agreement on Tuesday and finished the task on Wednesday. 

I was also given another task; working of the vision document. I worked Wednesday, Thursday and Friday on the document. In the start it was hard to understand exactly what should be in the vision document, but this got clearer after some time. 

We had our group meeting with the TA on Thursday 04.03.21 at 10 AM. Where we got some feedback from the TA. 
