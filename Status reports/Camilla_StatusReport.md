# Status reports:

## Week 8:
We decided which day of the week would be the best fit for the group meeting with our teacher assistant. With a few exchanges by mail with Peder (the TA) we agreed on 10 AM on Thursdays. 
Group meeting 25.02.21 we spent the first part of the meeting asking the TA questions and spent the rest of the meeting setting up the Collaboration Agreement and filling a few paragraphs. During the meeting we also set up our Git repository “PROG1004_2021_group23”. I wrote the Minutes of Meetings during the meeting.

We had another short meeting 26.02.21 for the people who were absent to discuss the day before, as well as delegating the team roles.

## Week 9
Since I was the team leader for this week, I gathered the feedback from our TA and teacher and decided who was going to implement what suggested changes. There was a little difficulty delegating the tasks for the 1st iteration due to uncertainty on how much workload each part of the iteration needed. 
We had a short group meeting 02.03.21 to decide what each of us were going to work on until Thursday so we would be able to show TA a first draft to give feedback on. 
I started working on the domain model. Me and agreed with Viktor to have a basics of the Domain model ready as soon as possible to base the Wireframe on. Due to a misunderstanding on my part the domain model looked too much like a User Case diagram and I started correcting that.
Group Meeting 04.03.21 with our TA. 
Since the Domain model was finished by Thursday, with some changes suggested by the teacher assistant, there was some freed-up space to help with the Vision document for the remainder of the week. 
Spent Thursday to Saturday filling in parts of the vision document, and worked on gathering the necessary files for the 1st iteration into one, large PDF.

