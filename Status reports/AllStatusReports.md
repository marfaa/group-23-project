# All status reports

## Viktor

### Week 8

Collaboration Agreement:
Worked on the collaboration agreement document and had a group meeting to discuss what parts each of us should work on within the document. We finished the collaboration agreement and divided into roles for the next weeks as noted in the agreement.
Thursday February 25th: 16:00 – 17:00 work on the collaboration meeting. 18:00-18:30 meeting about the collaboration meeting. 18:30 – 19:00 work on the collaboration meeting.

First iteration:
We were unsure about the contain of the documents that should be handed in for the first iteration. We asked the teaching assistant (TA) on Discord how to proceed and planned what we should finish before the meeting with him Thursday next week.
Note: I was not able to join the meeting with the TA the first week due to work schedule, changed my work schedule so I will able to join the meetings in the future.

Tasks for week 9:

- Finish all the documents for the meeting with the TA on Thursday so we can show our first drafts and get feedback on those.
- Do the tasks related to our roles as noted in the colleboration agreement.
- Take the notes during meetings.

### Week 9

Prototype:
Made the first draft of the prototype using Balsamiq on Tuesday and Wednesday taking into the account of Camilla's domain model. The first version of the prototype was too complicated and the other team members gave their insight on how to simplify the design.
I was unsure how the file could be shared with others to conduct user tests for our survey. The TA said that by exporting it as a PDF-file, the file will still be clickable and can easily be shared even if the recipient does not have Balsamiq.

Worked on this Tuesday 16:00-18:00, Wednesday 16:00-19:00 and Thursday 09:00 – 9:30.

Vision document:
We were unsure if we had to fill in the whole template for the vision document, the TA informed us that we should focus on the relevant sections. The professor will let us know the parts that we should have filled in, if anything is missing. I did the first parts of section 4 in the document.

Worked on this Thursday 11:00-13:00.

Meeting with TA:
We had a meeting with the TA on Thursday. I was the minute taker during the meeting. I also shared my prototype with him and got compliments on it.
The meeting was Thursday 10:00-10:45, I looked over the minutes I wrote and did some editing on it. I made a folder in git for all the meeting minutes and uploaded the minutes in it – this was done 10:45-11:00 the same day.

Meeting with the group:
We wanted to discuss the iteration 1 and how we should divide the tasks between ourselves. It was a bit confusing and stressful getting the iteration 1 so soon after the project started, but dividing the tasks made us a bit more confident that we would be able to complete all the tasks until Sunday.

Tuesday 20:00 – 20:30

Tasks for week 10:

- Edit the documents for iteration 1 according to feedback from the professor.
- Divide tasks for iteration 2.
- Tasks related to being the archive responsible.

### Week 10
Meeting with TA:
Wrote some action points during the meeting so we could easily divide tasks after the TA disconnected from the meeting.

Thursday 10:00 - 11:00.

Requirements section in the vision document:
Edited the requirement section to include the requirements listed in the project description document.
Thursdag: 11:00 - 12:30.

Archive responsibility:
Raised the question of requirements of the documents within iteration 1 and future documents with the TA during the meeting with him. Went through the different documents that we wanted to improve according to the TAs feedback.

Thursday 22:00 - 22:30.

Tasks for week 11:

- Divide future tasks.
- Edit documents according to doctor Ali's feedback.
- Tasks related to being the meeting organizer.

### Week 11
Wiki: 
Start making the WIKI with links to the work we already had done.

Thursday: 10:00 - 11:00.

Sequence Diagram:
Start making a sequence diagram, but noticed that it wouldn't make much sense to create it before the MVP was finished so I knew what functions was included in this version of the software.

Thursday: 11:00-12:00

Create invitation:
Created an invitation for the client meeting.
Monday: 16:30-17:00

Attend client meeting and a short catch up with the team:
Had a client meeting with Ali and TA, afterwards the team went through the feedback and planned what to do towards the next iteration.

Thursday: 14:00 - 15:00.

### Week 12
DevOPS:
Due to DevOPS no work was done related to the project.

Tasks for week 14:

- Finish up the tasks for the second iteration (WIKI + Sequence Diagram)

### Week 13
Easter holiday

### Week 14
Minute taking:
Taking minutes of the meeting with the TA and upload it to Git.

Thursday: 10:00-11:00

Wiki:
Create placeholder pages with links from the main page so that it is easy to add more content later. Write text to some of the pages in the WIKI.

Friday: 12:00 - 14:00

Sequence Diagram:
Finished an early version of the sequence diagram based on the current MVP. The sequence diagram have to be upgraded according to the final software.

Friday: 11:00 - 12:00

Tasks for week 15:

- Have a client meeting with Ali about the second iteration.
- Divide tasks for the final hand-in.

### Week 15
Client Meeting:
Made the PDF for the client meeting, prepared and lead the first part of the client meeting. Started implementing the feedback from the meeting.
Thursday: 8:00 - 12:00

Tasks for week 16:
- Make a draft of the use case diagram.

### Week 16
Use Case Diagram:
Made the first draft of the Use Case Diagram before the meeting with TA. 
Wednesday: 17:00 - 21:00

Could not attend the weekly meeting due to work.

Tasks for week 17:
- Finish the use case diagram, sequence diagram and wiki according to the latest changes in the program.

### Week 17
Use Case Diagram:
Changed the diagram according to TA's feedback.
Thursday: 16:00 - 18:00

WIKI:
Made the persistence, project structure, user manual and installation manual. Edited the WIKI according to changes in the program and linked to the newest version of the Vision document.
Monday: 16:00 - 18:00, Tuesday: 16:00 - 20:00 Wednesday: 16:00 - 20:00, Thursday: 8:00 - 10:00, 20:00-22:00

Weekly Meeting:
Participated in the weekly meeting, raised questions about the WIKI, use case diagram and sequence diagram. Had a short follow-up meeting the the rest of the team afterwards.
Thursday: 10:00-11:00

Sequence Diagram:
Added the newest functions in the program into the sequence diagram before the weekly meeting with the TA, afterwards edited it according to his feedback.
Wednesday: 20:00-22:00, Thursday 18:00-20:00.

## Camilla

### Week 8
We decided which day of the week would be the best fit for the group meeting with our teacher assistant. With a few exchanges by mail with Peder (the TA) we agreed on 10 AM on Thursdays.
Group meeting 25.02.21 we spent the first part of the meeting asking the TA questions and spent the rest of the meeting setting up the Collaboration Agreement and filling a few paragraphs. During the meeting we also set up our Git repository “PROG1004_2021_group23”. I wrote the Minutes of Meetings during the meeting.

We had another short meeting 26.02.21 for the people who were absent to discuss the day before, as well as delegating the team roles.

### Week 9

Since I was the team leader for this week, I gathered the feedback from our TA and teacher and decided who was going to implement what suggested changes. There was a little difficulty delegating the tasks for the 1st iteration due to uncertainty on how much workload each part of the iteration needed.

We had a short group meeting 02.03.21 to decide what each of us were going to work on until Thursday so we would be able to show TA a first draft to give feedback on.
I started working on the domain model. Me and agreed with Viktor to have a basics of the Domain model ready as soon as possible to base the Wireframe on. Due to a misunderstanding on my part the domain model looked too much like a User Case diagram and I started correcting that.

Group Meeting 04.03.21 with our TA:
Since the Domain model was finished by Thursday, with some changes suggested by the teacher assistant, there was some freed-up space to help with the Vision document for the remainder of the week.
Spent Thursday to Saturday filling in parts of the vision document, and worked on gathering the necessary files for the 1st iteration into one, large PDF.

### Week 10

Thursday: 10:00 - 11:00 - Group meeting
Received feedback on the 1st iteration submission from Peder at our group meeting on thursday. 

Organized the git repository by adding a prototype folder to hold the prototype as well as the report. Also added a folder for diagrams/models. Moved files to their respective folder.

### Week 11

Thursday: 11:00 - 12:00 - Client meeting
Presented the domain model and wrote the minutes of the meeting.

Had a short group meeting afterwards discussing the feedback.

### Week 12

DevOPS week.

### Week 13

Easter holiday, no group meeting.

### Week 14

Looked at the criteria for the 2nd iteration, and wrote out a list of tasks to split between the group. Made a milestone for the 2nd iteration, and added the issues to it. 

Made the MVP and uploaded it to the repository, aswell as a short list of the commands/features.

Thursday 10:00 - 11:00 - Group meeting 
Presented the MVP by demonstrating the features. Asked the TA for some details about the coming submission.

Wrote MVP_Features.md, a document about the features in the MVP.

### Week 15

10:00 - 11:00 : Presented MVP functions in client meeting, received feedback on some errors. Client requested the inclusion of the ability to store tasks in a file, so the software can start with tasks stored.

### Week 16

10:00 - 12:00 : Group meeting where we discussed the requirements for the final submission, decided to make more decision regarding delegating tasks for a meeting where more people were able to join.

Implemented changes suggested in client meeting.

### Week 17

16:45 - 17:30  Group meeting Monday: Decided who were going to oversee what files for the final submission.

Worked on the source code and made the class diagram by updating the domain model. 

10:00 - 11:00 Group meeting Thursday: Asked the teacher’s assistant questions and received feedback on what we had done so far.

Updated time list and status report uploaded zipped file of the source code and the class diagram.

## Maria

### Week 8:
We had our first meeting with our teaching assistant, Peder. We agreed on the meeting dates and time, made our GitLab project, and made all the templates for the time list, etc. We started to write the Collaboration Agreement and started to plan how we would distribute the team roles. 

The plan for next week is to write the first interaction report.

### Week 9:
Group meeting where we started planning how we were going to do the first interaction assignment. Everyone was given tasks to do within the meeting on Thursday. I was going to make the user test and prototype report. 

I could not start with my tasks until Viktor was done with the prototype. In the meantime, I updated and organized all my time lists, logs, and also made the template for the prototype report and user test. I also started writing down possible questions for the test. I had some questions for Peder about the report, which I wrote down. 

Group meeting with teaching assistant Peder. He answered all of our questions and showed us some pictures from his first interaction. After the meeting, I started making the questions for the user test. I showed several versions to the group, and they gave me their feedback. When the test was finished everyone shared it with their friends and family.  

### Week 10:
Worked on the prototype report and still tried to share the user test with friends and family. Finished the prototype report.

Wrote the prototype report in markdown and uploaded it to the Git project. I had some problems with the picture files of the charts and tables. Sent a message to Peder, and he said that he will show me how at our next meeting. Finished my log and time list for this week, and uploaded it to GitLab. 

### Week 11:
We had a client meeting with Ali, where he gave us feedback on the first iteration. I was the Minutes taker this week. After the meeting, I updated the Prototype Report after feedback from Ali and Peder. I also added milestones under Issues in our GitLab project. 

I also prepared for next week when I am group leader. 

### Week 12:
My week as group leader. I started the week by sending out a group message with a status update, what we need to do forward, and assigned some small assignments for everyone. 

DevOPS week. 

### Week 13: 
Easter holiday.

### Week 14:
Group meeting. Updated the 1st iteration after feedback from Ali and Peder. Also fixed the time list template and updated files to GitLab. 

### Week 15: 
Second client meeting with Ali. I also wrote the meeting minutes. After the meeting, I prepared for the next weeks and the final submission. 

### Week 16:
Was not able to attend the group meeting this week. The next day I used some time reading the meeting minutes and catching up. We had not yet assigned tasks for the last assignment, so I spent my time fixing the Gantt Chart and uploading it to GitLab.

### Week 17:
Group meeting on Monday where we assigned tasks for the last submission. I got the Main Report with Marthea. For the rest of the week, I worked very hard on this. On Friday I made the last updates on the report and also fixed some other technical issues before submitting it. I also finished my timelist, status report and the self reflection report.  

## Marthea

### Week 8

One of the first things we all figured out was which day and what time we would have our weekly group meetings with our teaching assistant (TA). We all agreed that at 10 AM on Thursdays would fit best for everyone. We had our first group meeting 25.02.21, where we asked the TA some questions about the collaboration agreement.

### Week 9

From our meeting with the TA the week before we got some feedback on the collaboration agreement, where we should make some changes. This week I was given the task to make the changes and add a few points to the Collaboration Agreement. I started working on the Collaboration Agreement on Tuesday and finished the task on Wednesday.

I was also given another task; working of the vision document. I worked Wednesday, Thursday and Friday on the document. In the start it was hard to understand exactly what should be in the vision document, but this got clearer after some time.

We had our group meeting with the TA on Thursday 04.03.21 at 10 AM. Where we got some feedback from the TA.

### Week 10

Group meeting with TA, we recieved feedback on the 1st iteration submission. The meeting took plact on Thursday 10.00 - 11.00.

### Week 11

Client meeting on Thursday 11.00 - 12.00.

We had a short meeting after, discussing the feedback from the client meeting. 

### Week 12

DevOPS week.

### Week 13

Easter holliday.

### Week 14

Group meeting on Thursday 10.00 - 11.00. The MVP was presented, and we got some information about the 2nd iteration. 

### Week 15

10.00 - 11.00, Meeting with client. Presenting of MVP functions. We received some feedback. 

### Week 16

Group meeting on Thursday 10.00-12.00, discussing requirements for the final submission. Not everyone could attend, so we decided to have another meeting where everyone could join.

### Week 17

Group meeting Monday 16.45 – 17.30. Decided who were going to do what for the final submission. 

Worked on the main report

Group meeting Thursday 10-11. Got feedback on what we had done so far. 

Updated time log and status report. 


## Jusuf

### Week 8

Collaboration Agreement:
We had a group meeting where we discussed how we should complete the collaboration agreement document. We agreed on dividing the work so that each member wrote something. We also discussed how our roles should be set on the upcoming weeks. 
I was not able to join our first team meeting with the TA because of work. The work schedule has been fixed according to our future team meetings. 

25.02.21:
18:00 - 19:00 group meeting
19:00 - 20:00 work on collaboration document
We had a non-official group meeting where we further discussed the collaboration agreement and how it should be written. We also discussed how the first iteration should be done. We contacted our TA on Discord and he gave us some instructions how what we should focus on. 
We came to an agreement on who should focus on what, and my job was to start on the vision document.
I was also assigned to be the meeting organizer, and our next meeting was setup to be: 04.03.21 at 10:00

### Week 9

03.03.21:
22:00 – 01:00 work on vision document
Worked on the vision document before our meeting that was scheduled to be the next day. The goal was to add as much information as possible so that the TA could provide some feedback on the work so far.

04.03.21: 
10:00 – 12:00 team meeting
12:00 – 13:00 work on vision document
On our team meeting, we got some feedback on the work that had been done till now. He informed us how the vision document should be written. What parts we should focus on our first iteration and some feedback on what had been done till now. 
After the meeting I worked some more on the vision document and changed some of the info based on the TA’s feedback.

05.03.21:
15:00 – 17:00 work on vision document
21:00 – 01:00 work on vision document
The team had added enough information to start formatting the document as a git format. I made a new document in our git folder named “Visiondoc.md”. I worked mostly on converting the google doc we made for vision document, to git format. 

## Week 10

11.03.21:
10:00 - 13:00 Team meeting where we recieved feedback on our first itereation from the TA Peder.
I made some changes on the vision document according to the feedback we got.


## Week 11

18.03.21:
11:00 - 14:00 We had a meeting with the client where we got feedback from the client and the assistant. We presented what we had done for iteration 1. From the feedback i worked more on the vision document. 

## Week 12

This week was used for the DevOPS tasks

# Week 13

Easter holiday so there was none group meetings.

# Week 14

07.04.21:
Started working on the MVP survey.

08.04.21:
10:00 - 11:00 Team meeting where got some information about how the second iteration should be delivered. Continued working on the MVP survey, camilla gave me the files for user testing.

10.04.21:
16:00 - 22:00 Finished the MVP survey and finished the user testing.

# Week 15

Got the Covid virus so i did not do anything the whole week. 

# Week 16

21.04.21:
18:00 - 20:00 Made some changes on the vision document to ask for guidence for the upcoming meeting.

22.04.21:
10:00 - 12:00 Had a Group meeting where we got feedback on some of the work that was done up to that point. We got more information on how the final report was supposed to be done

We divided some tasks between eachother and agreed on having another meeting on 

# Week 17

26.04.21:
16:45 - 18:45  Had a Team meeting where we discussed on how we should divide the tasks for the final main report. My job was to make som changes and finish up the Vision Document.

27.04.21:
16:00 - 20:00 Made plans for the changes on vision document. Started on some of the plans. 

28.04.21:
20:00 - 02:00 Worked on the Vision document. Added new tables with pictures. Added more information. 

29.04.21:
10:00 - 11:00 Team meeting where we discussed the different tasks we had done. Got some more information on what should/should'nt be on the final main report. 

12:00 - 20:00 Finished the Vision Document and made a PDF file.
