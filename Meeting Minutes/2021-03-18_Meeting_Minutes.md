# Meeting minutes 18.03.2021

### Presentation
After the presentation Ali reccommended that we spend some more time on building presentation skills
and have a more prepared presentation ready for the next client meeting.

### User testing
Overall the feedback on the user testing report were positive. A reccomandation for future reports was to pay attention too which dempgraphic the users are in when reviewing the answeres.
The example given was an answere that pointet out "too little space between lines", and if this was an answere from someone in the 60+ age group it could be a very important comment to keep in mind. 

### Git repository
The workload seemed to be evenly distributed and the feedback on the current organisation of files were positive. For future documents we should prioritize filetypes that works better when working/editing in the repository (markdown, etc.), and avoid services like Google Docs as much as possible.

Things to keep in mind:
- Add tasks frequently to boards/milestones
- Soon we need to start building the WIKI page for the repository

### Vision Document
Instead of formatting the tables with markdown we should add the tables inn as images, because when the files were converted to pdf-files the tables that were written didn't all show up. Also we need to add the version of the risk table with colors.
We should also add to the risk analysis chapter some plans for what we are going to do if some of the risks were to happened.

### MVP
It's not required for the MVP to feature all/most of the features listed in the assignment description, we should rather select some of the central features to start with. The important part of the MVP is that everything in the code is working.

### Iteration feedback
None of the feedback from iterations will be graded, except for the final one. The feedback will just be improvement suggestions to add afterwards. For future iteration there won't be feedback on the same parts that were commented on in the previous iteration.
When we get the full feedback on the 1st iteration we should start delegating the improvement tasks as soon as possible.
