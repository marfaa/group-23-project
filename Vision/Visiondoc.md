# Vision Document For Group 23

## Table of Contents

[Revision History](#revision-history)

1. [Introduction](#introduction)
    1. [Purpose](#purpose)
    2. [Scope](#scope)
    3. [References](#references)
    4. [Overview](#overview)
2. [Positioning](#goals)
    1. [Business Opportunity](#business-opportunity)
    2. [Problem Statement](#problem-statement)
    3. [Product Position Statement](#product-position-statement)
3. [Project Goals](#project-goals)
    1. [Efficiency Goals](#efficiency-goals)
    2. [Result Goals](#result-goals)
    3. [Process Goals](#process-goals)
4. [Stakeholder and User Description](#stakeholder-and-user-description)
    1. [Stakeholder Profiles](#stakeholder-profiles)
    2. [User Profiles](#user-profiles)
    3. [User Environment](#user-environment)
5. [Product Overview](#product-overview)
    1. [Product Perspective](#product-perspective)
    2. [Risk Analysis](#risk-analysis)
    3. [Cost Estimates](#cost-estimates)
6. [Product Features](#product-features)
    1. [Interaction Menu](#interaction-menu)
    2. [Creating Tasks](#creating-tasks)
    3. [Change Status and Priority](#change-status-and-priority)
    4. [Creating Categories](#creating-categories)
    5. [Display List of Tasks](#display-list-of-tasks)
    6. [Reorder Lists](#reorder-lists)
7. [Constraints](#constraints)
8. [Precedence and Priority](#precedence-and-priority)
9. [Requirements](#requirements)
    1. [Product Requirements](#product-requirements)
    2. [Documentation Requirements](#documentation-requirements)
10. [Feature Attributes](#feature-attributes)

### Revision History

![Image Text](RevisionHistory.PNG)

## Introduction

### Purpose

The purpose of this vision document is to define high level needs, features, collect and also analyze the different software requirements for izi-List. It focuses on the different needs of our target users and the capabilities needed by the stakeholders, and explains why these needs exist.

### Scope

The scope of this vision document is set mainly for the implementation but also the design for the software application Izi-list. The features, different uses, the structure and capabilities are also included.

### References

[1] [Lectures from PROG2021](https://ntnu.blackboard.com/ultra/courses/_23068_1/cl/outline) <br>
[2] [Vision Document template](https://ntnu.blackboard.com/ultra/courses/_23068_1/cl/outline) <br>
[3] [BalsamiQ prices for desktop version](https://balsamiq.com/buy/#d) <br>

### Overview

Following in this document:

**Chapter 2 Positioning**

- Gives a description of the business opportunity which states the needs of the project. 
- Table summarizing the problem statement.
- Table summarizing the product statement.

**Chapter 3 Project goals**

Provides several lists of different categories:

- Project goals that explain the objectives set for this project.
- Efficiency goals containing desired impact of the product.
- Result goals with plans for the products feature and quality, as well as plans for the usage of resources and time management.
- Process goals where it’s listed the desired outcomes that the team members will gain from working on this project.

**Chapter 4 Stakeholder and User Descriptions**

- Contains profiles of stakeholders and users with descriptions of the respective problems and the proposed solutions.
- Throughout the chapter there will also be an examination of the marketplace demographic, competitors and a review of noteworthy trends within the relevant market.

**Chapter 5 Product Overview**

- Explores the relations between the product and the user’s environment. 
- A summary of the products capabilities for a more practical method of addressing an issue without going through unnecessary details.
- Assumptions about what the product will depend on
- Measurement of the possible risks and their likelihood
- Quantified and un-quantifiable benefits provided by the product 
- Expected loss of direct costs, which estimates what resource spendage will be reduced by using the product.
- An estimation of the accumulated costs from creating the product
- Comparing the estimated costs with possible benefits
- Listings of licenses and installation requirements

**Chapter 6 Product features**

- Lists the features of the application with a description is intended for both people within and outside the project.

**Chapter 7 Constraints**

- Limits that comes with design, external capabilities and other dependencies

**Chapter 8 Precedence and Priority**

- Table categorizing features by their priority level.

**Chapter 9 Other Product Requirements**

- Containing the following in lists:
- All standards the product must comply to
- The necessary system requirements needed to support the software
- Performance required to support the software
- What is required from the environment

**Chapter 10 Documentation Requirements**

- The required documentation for the whole project

**Chapter 11 Feature Attributes**
- Status table over requests regarding the software.
- Info categorizing features according to the estimated effort requirements.

Chapter 2
## Positioning

### Business Opportunity

Our client has requested an easy, and effective to-do-list application. They are requesting a software where a user is able to register new tasks. When the new task is added, the software should keep all the relevant information like priority, task description, category, status, start and finish date, and deadline. They request that the user also should be able to mark the different tasks as done, change the tasks priority and reorder tasks.

### Problem Statement

![Image Text](ProblemStatement.PNG)

### Product Position Statement

![Image Text](ProductPositionStatement.PNG)

Chapter 3
## Project Goals

### Efficiency Goals

1. Create a time efficient and structured application for Time Management.
2. Reach the specific target group of specified in 2.2 Product Statement.
3. Increase the company’s personnel productivity by atleast 50%.

### Result Goals

Result goals describe what we want to achieve with the project. It is linked to the result and the end product. The result goals for the project are:
Providing an application with following features:

1. Ability to register different tasks added by user
    - Store registered tasks 
    - Sort tasks, with the option to reorder the list
    - Put tasks into different categories
    - Add a priority level to tasks
    - Provide display the status of each task, with the ability to change it
    - Ability for the user to add different categories
    - Add a start and end date to tasks


2. Provide an application with the following experience :
    - Application is compatible with the user’s environment
    - Usage of the applications features can easily be understood


3. Focus on the target group. We plan to focus on the needs and wishes of the target group to build a product beneficial for users within this group. To achieve this, we must research well and make sure that the product is tailored for the chosen target group.

4. Meeting submission deadlines. It is important that all assignments are submitted on time. Our goal is for all assignments to be approved without changing any deadlines. To achieve this, it is important that everyone adheres to their roles. It is also important that everyone attends meetings, in addition to working on their assigned tasks at home. The secretary holds the responsibility of having an overview of submission deadlines.

### Process Goals

Throughout the process of making this project, we have some goals we wish to complete.
Such as:

- Getting to know the team members better.We want to build trust in each other and learn from each other. To achieve this we want to have weekly meetings, an open dialog and to listen to each other’s suggestions.
- End up with a good product that all the members are happy with.
- Pass the subject
- Learn, and improve technical abileties
- Build and utilize each members strengths 

Chapter 4
## Stakeholder and User Description

### Stakeholder Profiles

A stakeholder is a party that has an interest in the development of the software. The identified stakeholders include the development team and a client that is also an expert, as well as a client/end-users.

**Table 4.1 Stakeholder Summary**

![Image Text](StakeholderProfiles.PNG)

### User Profiles
End Users

![Image Text](UserProfiles.PNG)

### User Environment

The application is meant to be used as a desktop software. It will be compatible with computer devices, and the operating systems: Windows, Linux and Mac OS.

### Stakeholder Profiles

**Client**

![Image Text](StakeholderProfilesClient.PNG)

![Image Text](StakeholderProfilesClient2.PNG)

**Development Team**

![Image Text](StakeholderProfilesDevelopmentTeam.PNG)

Chapter 5
## Product Overview

### Product Perspective

izi-List is intended to be a self-contained program installed/available on a user’s personal computer or laptop. The application will store, edit and read from text files generated within the local environment where izi-List is installed.

### Risk Analysis

Here we document conditions that may prevent the project to succeed. In addition, we make an assessment of the likelihood that the various risks occurring, and the consequences if they occur.

The risk factors we need to worry about are the ones in the lower right corner: likely and with serious consequences. Those we do not have to worry about are those located in the upper left corner: low risk, small impacts.

With this diagram, we briefly describe each risk factor:

(1 = lowest, 9 = highest)

![Image Text](RiskAnalysisID.PNG)

**How to resolve the different risk factors:**

**_A team member who is absent/sick:_** The team member have to notify the group in advance if they are not able to finish their work. The Group will distribute the tasks between eachother.

**_Unable to deliver in time because of technical issues:_** The team have to contact their TA/Professor and notify them as soon as possible about the technical issue.

**_Conflicts that arise within the team:_** If conflict arises within the team, the members will have to try to come to an agreement. If that is not possible then the current week's project leader makes the final decision. As a last resort the TA/Professor will be included to resolve the conflict. 

**_A lack of subject/technical experience:_** If there is a lack of experience the group will have to try on their own to complete the task. If the group still are not able to proceed with the tasks, they have to contact their TA for guidance.

**_Misunderstanding within the team:_** If there is misunderstanding within the team, the members have to come to a common understanding. The week's project team leader will have to explain what the group had agreed from the most recent group meeting. 

![Image Text](RiskAnalysisConsequences.PNG)

### Cost Estimates

![Image Text](CostEstimates1.PNG)

![Image Text](CostEstimates2.PNG)

Chapter 6
## Product Features

### Interaction Menu

Text which explains what the different command inputs will do on different screens. 

### Creating Tasks

The user is able to create new tasks by writing in a description of a task. To this task it will be added a priority rating, category, status, start time and deadline. 

### Change Status and Priority

The tasks registered by the user can be selected and given the option to change the status or the priority of the selected task. 

### Creating Categories

An ability for the user to create their own categories they can later tag the tasks with.

### Display List of Tasks

Ability to display registered tasks as a list with their information

### Reorder Lists

Ability to reorder the list of tasks (e.g. by priority, date) 

Chapter 7
## Constraints

This application will be created within the constraints of what can be made using the c++ language with software such as CodeBlocks or Visual Studio. Meaning that the advancement in how the user can interact with the software, and the graphic capabilities are limited to the terminal text.

The application is intended for usage on a personal computer, stationary or laptop, and will not be available on other types of end devices.

Chapter 8
## Precedence and Priority

![Image Text](PrecedenceAndPriority.PNG)

Chapter 9
## Requirements

### Product Requirements
The product should be a program adopting Graphic User Interface (GUI) following WCAG 2.1 Principle 1: Perceivable. Thus, the following requirements are to be met:
- the content is structured logically and can be navigated and read by a screen reader.
- use the proper markup for every feature, so the relationships between content are defined properly.
- not use colour as the only way to explain or distinguish something.
- use text colours that show up clearly against the background colour.

In addition, to ensure high usability and a good user experience the application is required to adopt Don Norman’s principles of interaction design. This includes the following principles:
- Visibility: The more visible an element is, the more likely users will know about them and how to use them.
- Feedback: Feedback should make it clear to the user what action has been taken and what has been accomplished.
- Constraints: Limiting the range of the possible interactions guides the user to the appropriate next action, this can be done through a simple interface.
- Mapping: It should be a clear relationship between the controls and the effect they have on the program.
- Consistency: This refers to having similar operations and similar elements for doing similar tasks.
- Affordance: The attributes of objects should allow people to know how to use them.

To ensure that the principles listed above are met, usability tests will be carried out twice. The first test will be a user test including a working prototype, while the second usability test will be centered around the minimum value product.
- Prototype: The prototype will be designed using Balsamiq to show the design and main interaction of the product.
- Minimum Value Product (MVP): The MVP will be programmed in C++ and will be a working prototype with just enough functionality to work as a proof of concept. Nonetheless, the MVP should not contain known bugs. Ideas for functionality that is not yet implemented will presented along with the MVP.

Chapter 10
### Documentation Requirements
All the documentation for this project will be uploaded on Gitlab under the main WIKI page and report.
- Domain Model
- Vision document
- Test report
- Wireframes
- Sequence diagram
- User case diagram
- Meeting notes and meeting invitations
- Time sheets
- Personal notes on the progress
- Main report

Chapter 11
### Feature Attributes
**Requests gotten from the user tests**
The requests are written on the left side. The requests comes from the user tests we had. The requests are the most frequent ones we had gotten.
"Possible" indicates what the team agreed on the different requests. If it is a possibilty for us to complete the request or not. 

![Image Text](StatusTable.PNG)

Using colors is not an possible option for us. We are limited to c++ coding, so the only colors we get to use is black background and white text.

Adding more information regarding the different options is something we all agreed on making. We think this will make the application more user-friendly for those that are less experienced with these kind of programs.

From our first user test report we got alot of comments saying that the design should be less messy. We took it as less compacted. We decided to seperate the different sections to make it easier for better readability. The seperation is done with lines and more spacing between the different sections.

We believe that what would take most effort would be creating a new/better design. We have to seperate us from the initial prototype we made. Overall the request for changes was not something we saw as a difficult task to complete.
