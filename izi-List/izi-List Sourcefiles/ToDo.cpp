//Header files made for the project.
#include "ToDo.h"
#include "AllConstants.h"
#include "GlobalFunctions.h"

#include <iostream>  // cout
#include <fstream>   // ofstream, ifstream
#include <string>    // string
#include <list>      // list
#include <algorithm> // transform
using namespace std;

/**
* Writes categories stored in the allCat vector to file
*/
void ToDo::writeFile_Cat()
{
    ofstream outFile("CATEGORIES.dta");     //Sets the file being used.

    //Checks if the file has been found.
    if (outFile)
    {
        //Writes explanation for number and the vector size.
        outFile << "Saved categories: " << allCat.size() << "\n";

        //For every category stored the program prints a line with
        //the category name.
        for (unsigned int i = 0; i < allCat.size(); i++)
            outFile << allCat[i] << '\n';

        //Closes the file
        outFile.close();

    //Error message if file is not found.
    } else cout << "\nERROR: \"CATEGORIES.dta\" not found";
}

/**
* Reads stored categories from file
*/
void ToDo::readFile_Cat()
{
    ifstream inFile("CATEGORIES.dta");  //Sets the file being used.

    string   buffer;    // For parts of the file that won't be added
                        // to the data structure.

       int   number;    // Count of how many categories are stored

    // Checks if the file has been found.
    if (inFile)
    {
        // Sets non-usable parts of the file as buffer
        // and sets the number as number of categories.
        inFile >> buffer >> buffer >> number; inFile.ignore();

        //A for-loop that repeats depending on the number of stored categories.
        for (int i = 0; i < number; i++)
          {
              //Reads the category string.
              getline(inFile, buffer);
              //Pushes it to the back of the category vector.
              allCat.push_back(buffer);
          }

        //Closes the file
        inFile.close();

    //Error message if file is not found.
    } else cout << "\nERROR: \"CATEGORIES.dta\" not found";
}

/**
* Reads stored tasks from file
*
* @see Task::Task(...)
*/
void ToDo::readFile_Task()
{
    ifstream inFile("TASKS.dta"); //Sets file being used.

    string   buffer;    // For parts of the file that won't be added
                        // to the data structure.

       int   number;    // Amount of tasks that will be read.

       Task* fileTask;  // Type of object being made.

    // Checks if the file has been found.
    if (inFile)
    {
        //Reads amount of tasks
        inFile >> buffer >> buffer >> number;

        //Sets up a for-loop that repeats for every task stored.
        for (int i = 0; i < number; i++)
           {
               //Sends file too Task constructor.
               fileTask = new Task(inFile);
               //Pushes the new task to the back of the vector.
               allTasks.push_back(fileTask);
           }

      //Closes the file.
      inFile.close();

    //Error message if file is not found.
    } else cout << "\nERROR: \"TASKS.dta\" not found";
}

/**
* Writes tasks stored in the allTasks vector to file
*
* @see Task::tooFile(...)
*/
void ToDo::writeFile_Task()
{
    ofstream outFile("TASKS.dta"); //Sets file being used.

    // Checks if the file has been found.
    if (outFile)
    {
        //Writes explanation for number and the vector size.
        outFile << "Saved tasks: " << allTasks.size() << "\n\n";

        //For every task stored the program calls the tasks function for
        //writing into a file.
        for (unsigned int i = 0; i < allTasks.size(); i++)
          { allTasks[i]->tooFile(outFile); outFile << "\n\n"; }

      //Closes the file
      outFile.close();

    //Error message if file is not found.
    } else cout << "\nERROR: \"TASKS.dta\" not found";
}

/**
* Adds a new task
*
* @see Task::Task()
*/
void ToDo::addTask()
{
    Task* newTask; //Type of object being made.

    //Clears the screen and prints out a headline for the function.
    cout << "\n\tNEW TASK\n"; drawLine(40);

    //Creates a new Task object and pushes it to the back of the vector.
    newTask = new Task();

    //Asks the user if they would like to add a category
    if (readInt("\nAdd category? [ 1 - Yes | 0 - No ]", 0, 1))
        newTask->addCategory(readCat(false));

    //Pushes new task to the back of the vector.
    allTasks.push_back(newTask);

}

/**
* Changes the status of a chosen task
*
* @see Task::changeStatus();
* @see Task::getNr();
*/
void ToDo::completedTask()
{
    int selected; //User selects which task to change.

    //Sees if there are any tasks registered.
    if (!allTasks.empty())
    {
    //User is asked to choose a task by their task number.
     selected = readInt("\nSelect task by typing in the task number"
                        "", 1, taskCount);

     //Goes through the content of vector and changes status when the program
     //finds a task with the corresponding assigned number.
     for ( auto itTask = allTasks.begin(); itTask !=allTasks.end(); itTask++ )
        {
           if (selected == (*itTask)->getNr() )
           { (*itTask)->changeStatus(); }
        }

    //Message printed if vector is empty.
    } else cout << "\n\tTO DO LIST EMPTY";
}

/**
* Writes out all registered tasks
*
* @param sortBy - enum used to sort task list
* @param resetOrder - true if user have chosen the enum ByCategory
*
* @see ToDo::printBy_Priority()
* @see ToDo::chooseCategory(...)
* @see ToDo::printBy_Date(...)
* @see Task::setNr(...)
*/
void ToDo::printTasks(const Sorting sortBy, bool resetOrder)
{
    //If there is no category stored the no category tag is pushed to the back
    if (!allCat.size()) allCat.push_back(NOCAT);

    //Checks if there are any stored tasks.
    if (allTasks.size())
    {
        //Resets the assigned number for all tasks.
        for (unsigned int i = 0; i < allTasks.size(); i++)
         { allTasks[i]->setNr(0); }

        //Resets number tasks counted in sorting functions.
        taskCount = 0;

        cout << "\n\t\tTO DO\n"; drawLine(80); cout << '\n';

        //Uses sent enum variable and calls the corresponding function.
        switch (sortBy)
         {
         case byPriotiy:  printBy_Priority();             break;
         case byCategory: chooseCategory(resetOrder);     break;
         case byStart:    printBy_Date(sDate);            break;
         case byDeadline: printBy_Date(eDate);            break;
         default: cout << "writeTask() ERROR";
         }

    //If there are no tasks stored a message is printed.
    } else cout << "\n\tTO DO LIST EMPTY";
}

/**
* Prints out all of the content of the category vector
*/
void ToDo::printCategories()
{
    cout << "\n STORED CATEGORIES:\n";

     for (unsigned int i = 0; i < allCat.size(); i++ )
        //For every two category printed a new line is added.
     {   if ( i % 2 == 0 ) cout << '\n';
         cout << "\t" << i+1 << ". " << allCat[i]; }

    cout << '\n';
}

/**
* User chooses which of the stored categories to sort by if necessary
*
* @param newChoice - true if the user has chosen to sort by category
* @see printCategories()
* @see printBy_Category(...)
*/
void ToDo::chooseCategory(bool newChoice)
{
    //If the user choose to sort by category
    if ( newChoice )
    {
        //Categories are printed and user is asked too choose one
        printCategories();
        drawLine(80);
        //Updates the which category is chosen.
        chosenCat = allCat[readInt("\n Choose a category", 1, allCat.size())-1];
    }

    // Sends either the updated category choice or the previously chosen
    // category.
    printBy_Category(chosenCat);
}

/**
* Prints all tasks in descending order by priority
*
* @see Task::printBy_Priority()
* @see Task::getNr();
*/
void ToDo::printBy_Priority()
{
    int prio1, prio2, prio3; //Amount of tasks with each priority.

    //All priority counters are set too 0.
    prio1 = prio2 = prio3 = 0;

    cout << '\n'; drawLine(25);
    cout << "\n [3/3] PRIORITY";
    cout << '\n'; drawLine(25); cout << '\n';

    //For-loop that goes through all tasks to find tasks with
    //a priority rating of 3.
    for ( auto itTask = allTasks.begin(); itTask !=allTasks.end(); itTask++ )
    {
      if ((*itTask)->getPriority() == 3 ) //For each task found.
      {
         //Counters increased by one.
         taskCount++; prio3 ++;

         //Assignes a number to a task with counter.
         (*itTask)->setNr(prio3);
         //Prints the task.
         cout << "\n [Task nr." << prio3 << "]\t";
         (*itTask)->printBy_Priority();
      }
    }

    //If no tasks was found a message is printed.
    if (!prio3) cout << "\n No tasks\n";


    cout << '\n'; drawLine(25);
    cout << "\n [2/3] PRIORITY";
    cout << '\n'; drawLine(25); cout << '\n';

    // Sets next counter as the same value as previous counter so that
    // if the previous for-loop found one or more tasks the assigned number
    // don't start at 0.
    prio2 = prio3;

    // Rest of function repeats the for-loop pattern, but for the other
    // priority ratings.

    for ( auto itTask = allTasks.begin(); itTask !=allTasks.end(); itTask++ )
    { if ((*itTask)->getPriority() == 2 )
      {
         taskCount++; prio2++;

         (*itTask)->setNr(prio2);
         cout << "\n[Task nr." << prio2 << "]\t";
         (*itTask)->printBy_Priority();
      }
    }

    if (!prio2) cout << "\nNo tasks\n";

    cout << '\n'; drawLine(25);
    cout << "\n [1/3] PRIORITY";
    cout << '\n'; drawLine(25); cout << '\n';

    prio1 = prio2;

    for ( auto itTask = allTasks.begin(); itTask !=allTasks.end(); itTask++ )
    { if ((*itTask)->getPriority() == 1 )
      {
          taskCount++; prio1++;

         (*itTask)->setNr(prio1);

         cout << "\n[Task nr." << prio1 << "]\t";
         (*itTask)->printBy_Priority();
      }
    }

    if (!prio1) cout << "\nNo tasks\n";
}

/**
* Prints out tasks tagged with a chosen category
*
* @param catChoice - The default category or one chosen by the user.
* @see Task::getCat()
* @see Task::setNr(...)
* @see Task::printBy_Category()
*/
void ToDo::printBy_Category(const string catChoice)
{
    string catUpper; //To convert string to uppercase

    // Sets local string to the sent category name and converts all letters
    // too uppercase.
    catUpper  = catChoice;
    std::transform(catUpper.begin(), catUpper.end(), catUpper.begin(), ::toupper);

    cout << '\n'; drawLine( 25 + catUpper.size() );
    cout << "\n TASKS MARKED AS " << catUpper;
    cout << '\n'; drawLine( 25 + catUpper.size() ); cout << '\n';

    //Goes through all stored tasks.
    for (unsigned int i = 0; i < allTasks.size(); i++)
    {
        //If the tasks category matches the sent string
        if ( allTasks[i]->getCat() == catChoice)
        {
            //Counter is increased by one.
            taskCount++;
            //Task is assigned a number.
            allTasks[i]->setNr(taskCount);
            //Task is printed.
            cout << "\n [Task nr." << taskCount << "]\t";
            allTasks[i]->printBy_Category();
        }
    }

    //Message is printed if no tasks is tagged with the chosen category
    if (!taskCount) cout << "\n\tNo tasks\n";
}

/**
* Prints task list in ascending order by date
*
* @param option - if the chosen list order is by start date or deadline
* @see Task::getDate()
* @see Task::setNr(...)
* @see Task::printBy_Date(...)
*/
void ToDo::printBy_Date(const SortDate option)
{
    list <string> allDates; //For storing dates

    //Adds all dates from stored tasks to list.
      for (unsigned int i = 0; i < allTasks.size(); i++)
        { allDates.push_back(allTasks[i]->getDate(option)); }

    //Sorts the list in ascending order and removes duplicates.
    allDates.sort();
    allDates.unique();

    //Goes through all dates in list.
     for (auto itDate = allDates.begin(); itDate != allDates.end(); itDate++)
       {
         cout << '\n'; drawLine(25);
         //Prints every unique date.
         if (option == sDate) cout << "\n START DATE ";
         else cout << "\n DEADLINE ";
         //Prints the unique date.
         cout << allTasks[0]->printDate(*itDate);
         cout << '\n'; drawLine(25); cout << '\n';

         //Goes through all stored tasks
          for ( unsigned int i = 0; i < allTasks.size(); i++ )
           {
               //Looks for tasks with a date that matches the one pointed to
               //in the date list.
               if (allTasks[i]->getDate(option) == *itDate )
               {
                   //Counter is increased by one.
                   taskCount++;
                   cout << "\n [Task nr." << taskCount << "]\t";

                   //Assign corresponding task a number and
                   //prints the task.
                   allTasks[i]->setNr(taskCount);
                   allTasks[i]->printBy_Date(option);
               }
           }
        }

    //Clears the list of dates.
    allDates.clear();
}

/**
* Function that reads, creates and returns category names
*
* @see ToDo::compareCat(...)
* @return tempCat - category name
*/
string ToDo::readCat(const bool create)
{
    string tempCat;     //Category name

    //Program reads in category name from user.
    cout << "\nCategory name: "; getline(cin, tempCat);

    //Checks if the name typed in matches a category stored in vector
      if (!compareCat(tempCat))
        {
            //If the category does not exist the user is asked if they wish to
            //make the typed in name a new category.
            cout << "\n Category \"" << tempCat << "\" does not exist.";

            if (readInt("\nDo you want to create this category? [ 1 - Yes | 0 - No ", 0, 1))
            {
              //If the user chooses "Yes" the name is added to the vector.
                allCat.push_back(tempCat);
              //The program returns the typed in name.
                return tempCat;

            //If the user chooses "No" a message is printed and the const
            //NOCAT is returned
            } else { cout << "\nNo Category set"; return NOCAT; }

        //If the name typed in by user matches is already stored in vector
        //the name is returned.
        } else {
                //If the function is called to create a new task there will
                //be a message notifying the user that category already exists.
                if (create)
                 { cout << '\n' << tempCat << " is already created"; }
                 return tempCat;
               }

    //To secure the code, not intended to ever be returned.
    return tempCat;
}

/**
* Function that checks if the sent category name is already created
*
* @param checkName - category name that is being compared
* @return true - if the name is already stored
          false - if the name is not already in the vector
*/
bool ToDo::compareCat(const string checkName)
{
    //Goes through every stored category
    for (unsigned int i = 0; i < allCat.size(); i++)
    {
        //If a category has the same name as the string sent to the function
        //it returns true.
        if (allCat[i] == checkName) return true;
    }

    //If no category was found in the for-loop the program returns false.
    return false;
}
