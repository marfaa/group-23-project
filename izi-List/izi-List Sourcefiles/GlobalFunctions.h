#ifndef __GLOBALFUNCTIONS_H
#define __GLOBALFUNCTIONS_H

//Access to enum types.
#include "AllEnums.h"

//More information in GlobalFunctions.cpp
void writeOptions();
void drawLine(const int length);
Sorting sortingChoice();

//Borrowed function, more information in the functions definition.
int readInt(const char* text, const int min, const int max);

#endif
