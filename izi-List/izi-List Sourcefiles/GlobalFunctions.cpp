//Header files made for the project.
#include "GlobalFunctions.h"
#include "AllConstants.h"
#include "ToDo.h"

#include <cstdlib>  //atoi
#include <iostream> //cout, cin
using namespace std;

// ---------------------------------------------------------------------------
//      DEFINITION OF GLOBAL FUNCTIONS
// ---------------------------------------------------------------------------

/**
* Writes out the options for the start menu
*/
void writeOptions()
{
    cout << "\n\tOptions:\n" <<
            "\t\t 1 - New task \t\t 2 - Change task status\n" <<
            "\t\t 3 - Reorder tasks \t 4 - Create new category\n\n" <<
            "\t\t\t\t 5 - Quit\n";

}

/**
* Draws a line inbetween texts
*
* @param length - given length og the line
*/
void drawLine(const int length)
{
    //Sets up a for-loop that prints '_' the amount given.
    for (int i = 0; i < length; i++) cout << '_';
}

/**
* Returnes an enum type chosen by user
*
*/
Sorting sortingChoice()
{

    cout << "\n Sorting options:" <<
            "\t1 - By priority \t 2 - By category \n" <<
            "\t\t\t3 - By Start \t\t 4 - By deadline\n";

    drawLine(80);

    //Asks the user to choose one of the options and returns the
    //corresponding sorting choice.
    switch(readInt("\n\n Command", 1, 4))
    {
        case 1 : return byPriotiy;      break;
        case 2 : return byCategory;     break;
        case 3 : return byStart;        break;
        case 4 : return byDeadline;     break;
    }

    //To secure the code, never intended to be returned.
    return byStart;
}

// ---------------------------------------------------------------------------
//      BORROWED FUNCTIONS
// ---------------------------------------------------------------------------

/**
* Borrowed function from lesData3.h given in the
* PROG1003 course, used to read and return
* an integer within an given interval. The function also checks if the user
* has typed in a letter.
*
* @param text - given text that prints out before reading the number.
* @param min - minimum allowed integer.
* @param max - maximum allowed integer.
*
* @return the integer typed in by the user, as long as it passes the checks.
*/
int readInt(const char* text, const int min, const int max)
{
    char buffer[MAXCHAR]; //Used to read input so it can be checked for letters.
    int  number;          //The value returned if possible.
    bool error;           //Check for errors in input.

    //Sets up a do-while loop.
    do {
        error = false;

    //Prints out the given text and minimum/maximum integer.
        cout << text << " (" << min << " - " << max << "):  ";
        cin.getline(buffer, MAXCHAR);

    //Converts buffer to an int value as long as the input consists of integers.
        number = atoi(buffer);

    //If non-integers have been found.
        if (number == 0 && buffer[0] != '0')
        // Error is set to true and a messange is printed.
        {  error = true; cout << "\nPlease enter a number\n\n";  }

    //If the number is under minimum or over maximum.
        else if (number < min || number > max)
        //Error is set to true and a message is printed.
        { error = true; cout << "\nPlease enter a number within the interval"
                             << " (minimum - maximum)\n\n";  }

      //As long as an error have been found, the do-while loop starts again.
    } while (error);

    //If no errors have been found the input is returned.
    return number;
}
