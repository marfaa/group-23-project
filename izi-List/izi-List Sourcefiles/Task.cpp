//Header files made for the project.
#include "AllConstants.h"
#include "AllEnums.h"
#include "Task.h"
#include "GlobalFunctions.h"

#include <fstream>  //ifstream, ofstream
#include <iomanip>  //setw
#include <iostream> //cout, cin
using namespace std;

/**
* Initiates the object
*
* @see Task::readTask()
* @see Task::setPriority()
*/
Task::Task()
{
    startDate = endDate = "";

    //Reads inn content for the task.
    readTask();
    setPriority();

    //sets the status to false ("not completed").
    status = false;
    //sets the category as "noSetCategory"
    category = NOCAT;

}

/**
* Constructor that reads datamembers in from file
*
* @param in - file the program reads in from
*/
Task::Task(std::ifstream & in)
{
    string buffer; //for parts of file not to be added to object.

    in.ignore();

    //Reads description.
    in >> buffer; in.ignore(); getline(in, description);

    //Reads dates.
    in >> buffer; in.ignore(); getline(in, startDate);
    in >> buffer; in.ignore(); getline(in, endDate);

    //Reads status
    in >> buffer >> priority >> buffer >> status;

    //Reads category.
    in >> buffer; in.ignore(); getline(in, category);
}

/**
* Writes a tasks datamembers to file
*
* @param out - file the program writes too
*/
void Task::tooFile(ofstream & out)
{
  out << "Description: " << description <<
         "\nStart: " << startDate <<
         "\nEnd: "  << endDate <<
         "\nPriority: " << priority << " Status: " << status <<
         "\nCategory: " << category;
}


/**
* Reads the description and date for a task
*
* @see Task::dateToString(...)
* @see Task::printDate(...)
*/
void Task::readTask()
{
    int tempDay, tempMonth, tempYear; //used to make the date string.

    //Reads description
    cout << "\nWhat is the task?: ";
    getline(cin,description);

    cout << "\nType in a start date:";
    //Reads in dates within given intervalls.
    tempDay   = readInt("\nDay", MINDAY, MAXDAY);
    tempMonth = readInt("\nMonth", MINMONTH, MAXMONTH);
    tempYear  = readInt("\nYear", MINYEAR, MAXYEAR);

    //Creates a start date string from the read integers.
    startDate = dateToString(tempDay, tempMonth, tempYear);

    //Same process for reading the deadline/end date.
    do {
    cout << "\nType in an end date:";

    tempDay   = readInt("\nDay", MINDAY, MAXDAY);
    tempMonth = readInt("\nMonth", MINMONTH, MAXMONTH);
    tempYear  = readInt("\nYear", MINYEAR, MAXYEAR);

    endDate = dateToString(tempDay, tempMonth, tempYear);

    //If a deadline earlier than the start date is read a message is printed
    //and the user have to type in a new one.
    if (startDate > endDate)
    { cout << "\nEnd date must be later than start date"
           << "Start date: " << printDate(startDate); }

    } while (startDate > endDate);

}

/**
* Gives the task a priority rating.
*/
void Task::setPriority()
{
    //User asked choose the priority within an interval.
    priority = readInt("\nSet a priority level", MINPRIO, MAXPRIO);
}

/**
* Assigns a number to task
*
* @param sentNr - number that is assigned to task
*/
void Task::setNr(const int sentNr)
{ assignedNr = sentNr; }

/**
* Changes/adds the tasks category
*
* @param setCat - The category name sent to function
*/
void Task::addCategory(const string setCat)
{   category = setCat;   }

/**
* Changes the boolean value 'status'
*/
void Task::changeStatus()
{
    //If "status" is false, it will be set to true
    if (!status) status = true;
    //If "status" is true, it will be set to false
    else status = false;
}

/**
* Prints all of the tasks datamember except number and priority
*
* @see Task::printDate(...)
*/
void Task::printBy_Priority()
{
  cout << '\t' << left << setw(15) << description <<
          "Start: " << setw(15) << printDate(startDate) <<
          "\tDeadline: " << setw(15) << printDate(endDate) << left <<
          "\n\t\t\tCategory: " << setw(10) << category <<
          right << "\t\t\t" << ((status) ? "C" : "Not c") << "ompleted\n";
}

/**
* Prints all of the tasks datamember except number and category
*
* @see Task::printDate(...)
*/
void Task::printBy_Category()
{
  cout << '\t' << left << setw(15) << description <<
          "Start: " << setw(15) << printDate(startDate) <<
          "\tDeadline: " << setw(15) << printDate(endDate) << left <<
          "\n\t\t\tPriority [" << priority << "/3]\t" <<
          right << "\t\t\t" << ((status) ? "C" : "Not c") << "ompleted\n";
}

/**
* Prints all of the tasks datamember except number and the date the tasks
* are sorted by.
*
* @see Task::printDate(...)
*/
void Task::printBy_Date(const SortDate option)
{
  string dateSt = ""; //Initiates the string as blank

  //Checks if the date will be printed by start date (sDate) or end date (eDate)
  if (option == sDate)

    //Creates a string with the date the task is NOT sorted by
    //since the chosen date to sort by will be printed in ToDo::printBy_Date(..)
        dateSt = "Deadline: " + printDate(endDate);
  else dateSt = "Started: " + printDate(startDate);

   cout << '\t' << setw(15) << left << description <<
                   setw(20) << dateSt << right <<
                   setw(15) << "Priority [" << priority << "/3]\t" << left <<
                   "\n\t\t\tCategory: " << setw(10) << category <<
                   right << "\t\t\t" << ((status) ? "C" : "Not c") << "ompleted\n";

}

/**
* Returns a one of the date strings
*
* @param option - decides which string is returned
*/
string Task::getDate(const SortDate option)
{
    if (option == eDate) return endDate;
    return startDate;
}

/**
* Prints out a more reader friendly date from one of the stored dates
*
* @param dateString - the string that is transformed
* @return datePrint - the transformed string
*/
string Task::printDate(const string dateString)
{
    string y, m, d,     //strings to separate the date values.
           datePrint;

    y = m = d = "";

    //First four symbols is added to y(ear) string.
    for (unsigned int i = 0; i < 4; i ++)
    y += dateString[i];

    //Symbol number 5 and 6 is added to m(onth) string.
    for (unsigned int i = 5; i < 7; i ++)
     { m += dateString[i]; }
    //If dash is found the month is less than 10 and a 0 is added to the front.
    if (m[1] == '-') { m[1] = m[0]; m[0] = '0'; }

    //Rest of the string is added to the d(ay) string.
   for (unsigned int i = 7; i < dateString.size(); i ++)
    //Only adds digits to day string
     { if (isdigit(dateString[i])) d += dateString[i]; }

    //If the day strings length is less than two a 0 is added to the front.
    if (d.size() < 2) d = "0" + d;

    //Combines everything to one string and returns it.
    datePrint = d + "." + m + "." + y;
    return datePrint;
}

/**
* Uses integer sent by user to create a date string
*
* @return dateAsString - string representing start or end date.
*/
string Task::dateToString(const int day, const int month, const int year)
{
    string dateAsString;

    dateAsString = to_string(year) + "-" + to_string(month) + "-" + to_string(day);

    return dateAsString;
}
