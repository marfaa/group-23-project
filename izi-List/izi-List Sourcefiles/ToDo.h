#ifndef __TODO_H
#define __TODO_H

#include "Task.h"
#include "AllEnums.h"

#include <string>   //string
#include <vector>   //vector

/**
* The to do list containing tasks and categories
*/
class ToDo
{
private:
    int         taskCount;  //Counts tasks printed in list
    std::string chosenCat;  //What category to sort task list by

    std::vector<Task*>       allTasks; ///< all added tasks.
    std::vector<std::string> allCat;   ///< all added categories.
public:
    //More in depth information in ToDo.cpp

    //Functions for use of files
    void readFile_Cat();
    void writeFile_Cat();
    void readFile_Task();
    void writeFile_Task();

    //Functions for use of categories
    void printCategories();
    void chooseCategory(bool newChoice);
    std::string readCat(const bool create);
    bool compareCat(const std::string checkName);

    //Functions for use of tasks.
    void addTask();
    void completedTask();

    //Functions for printing different task lists.s
    void printTasks(const Sorting sortBy, bool resetOrder);
    void printBy_Priority();
    void printBy_Category(const std::string catChoice);
    void printBy_Date(const SortDate option);
};

#endif
