#ifndef __TASK_H
#define __TASK_H

#include "AllEnums.h"

#include <string>   //string
#include <fstream>  //ifstream, ofstream

class Task
{
private:
    int          assignedNr,
                 priority;

    std::string  startDate,
                 endDate,
                 description,
                 category;

    bool         status;    //If the task is completed or not.

public:
    //More in depth information in Task.cpp

    //Constructors
    Task();
    Task(std::ifstream & in);

   //Returns different private datamembers
   int getPriority() const { return priority; }
   int getNr() const { return assignedNr; }
   std::string getCat() { return category; }
   std::string getDate( const SortDate option );

   //For adding/changing datamembers
   void tooFile(std::ofstream & out);
   void readTask();
   void setPriority();
   void setNr(const int sentNr);
   void changeStatus();
   void addCategory(const std::string setCat);
   std::string dateToString(const int day, const int month, const int year);
   bool checkEndDate(const int dEnd, const int mEnd, const int yEnd);

   //For printing different task information.
   void printBy_Priority();
   void printBy_Category();
   void printBy_Date(const SortDate option);
   std::string printDate(const std::string dateString );


};

#endif
