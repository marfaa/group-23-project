//Header files made for the project.
#include "AllEnums.h"
#include "ToDo.h"
#include "GlobalFunctions.h"

#include <iostream> //cout
using namespace std;

ToDo taskList;
extern ToDo taskList;

/**
* Main function
*/
int main()
{
    int command;     //User input to navigate the switch menu
    bool ifCategory;
    Sorting choice,
            tempChoice;

    //Reads stored tasks and categories from files
    taskList.readFile_Cat();
    taskList.readFile_Task();

    //Sorts list by start date as a default choice.
    choice = byStart;

    //Sets up a do-while loop
    do
    {
      //Prints out the tasks (if there are any).
        cout << '\n'; drawLine(80);

        taskList.printTasks(choice, ifCategory);
        ifCategory = false;

        cout << '\n';drawLine(80);
    //Prints out the aviable options
        writeOptions();
         drawLine(80);

    //User is asked to type in one of the options
        command = readInt("\nType in choice", 1, 5);

    //A switch statment to send the program to the chosen option
        switch(command)
         {
           //Creates new task
           case 1: taskList.addTask();         break;

           //Changes task status
           case 2: taskList.completedTask();   break;

           //Reorder task list
           case 3: { tempChoice = sortingChoice();

                     //If the user chooses byCategory ToDo::writeTasks(...)
                     //will prompt the user to choose a category.
                     if (tempChoice == byCategory) ifCategory = true;

                     choice = tempChoice;
                    }                          break;

           // User types in a category name and if it doesn't exist
           // the user is given an option to create the typed in name.
           case 4: taskList.readCat(true);         break;

           default: cout << "\nERROR";
         }

   //As long as the user doesn't choose option 5 the do-while loop continues.
    } while (command != 5);


   //Writes to files containing tasks and categories.
    taskList.writeFile_Cat();
    taskList.writeFile_Task();

   //Clears the screen when out of the do-while loop.
    cout << flush; system("CLS");
   //Prints out a goodbye-message.
    cout << "\n\n\tizi-List is closing...\n\t\tGoodbye!\n\n";
    return 0;
}

